﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Oda08Reto1 : Challenge
    {
        // Enumeration
        public new enum Action
        {
            ShowEnemy, ShowProfessor,EndChallenge
        }
        // Attributes
        private UnityEngine.GameObject goInicio, goProfesor,goProfesorLast, goGloboProfesor, goLaboratorio, goEnemigo, goPociones, goReceta, goMezcla, goAtaque, goMezclaAnimation, goZonaPreparacion, goIngredientes, goZonaBloqueo, goMedidorLiquidos, goMedidorSolidos;
        private Oda08Indice goMenu;
        private Oda08Reto2 goReto2;
        private Button btnMezcla1, btnMezcla2, btnMezcla3, btnMezcla4;
        private Button btnPuerta, btnPocion1, btnPocion2, btnPocion3, btnPocion4, btnMezclar, btnAguaCactus, btnAguaManantial, btnAguaPozo, btnAguaTriangulo, btnAlasUnicornio, btnAureola, btnCorteza, btnEspina, btnLagrimas, btnLimadura, btnMaderaBarco, btnPlumaAveztruz, btnSalivaCaracol, btnSalivaTroll, btnTierraTunel, btnUniaCorrecaminos, btnUniaLeon, btnAceptarLiquidos, btnAceptarSolidos, btnBorrarSolidos, btnTeclado0, btnTeclado1, btnTeclado2, btnTeclado3, btnTeclado4, btnTeclado5, btnTeclado6, btnTeclado7, btnTeclado8, btnTeclado9;
        private Image imgInterfaz, imgReceta, imgAreaInteractiva;
        private Animator animPuerta, animZoom,animMezclaBuena,animMezclaMala,animMezclaGeneric,animVidaEnemigo, animVidaAlumno;
        private bool esRetoActivo, esZoomInTerminado,banPopUp;
        private Slider sldMedidorLiquidos;
        private List<string> lsIngredientes;
        private List<ERecipe> loRecetas;
        private int numPocionActual, numIngredientesServidos, contPocionServidas,numAtaquesEnemigo, numAtaquesAlumno,numAtaquesTotal;
        private string ingredienteActual, proporcionSolido;
        private Text txtReceta, txtProporcionSolido;
        private bool[] banMezclaAnimationStop;
        public System.Random rndDosis,rndLiquidos, rndSolidos;
        // Constructor
        public Oda08Reto1() : base("Reto1"){}
        // Use this for initialization
        protected override void Start()
        {
            lsIngredientes = new List<string> {
                "SalivaCaracol","AguaCactus", "AguaManantial", "AguaPozo", "AguaTriangulo", "AlasUnicornio", "Aureola",
                "Corteza", "Espina", "Lagrimas", "Limadura", "MaderaBarco", "PlumaAveztruz", 
                "SalivaTroll", "TierraTunel", "UniaCorrecaminos", "UniaLeon"
            };
            contPocionServidas = 0;
            numAtaquesEnemigo = 0;
            numAtaquesAlumno = 0;
            numAtaquesTotal = 0;
            //test
            //PlayerPrefs.SetInt ("estrellasODA08_ODA08",0);
            banPopUp = false;
            banMezclaAnimationStop = new bool[4];
            for (int i = 0; i < 4; i++) {
                banMezclaAnimationStop [i] = false;
            }
            rndLiquidos = new System.Random();
            rndSolidos = new System.Random();
            rndDosis = new System.Random();

            goMenu = transform.GetComponent<Oda08Indice>();
            goReto2 = transform.GetComponent<Oda08Reto2> ();

            goInicio = Canvas.transform.Find("Inicio").gameObject;
            btnPuerta = goInicio.transform.Find("btnPuerta").GetComponent<Button>();
            btnPuerta.onClick.AddListener(MostrarLaboratorio);
            animPuerta = btnPuerta.GetComponent<Animator>();
            animPuerta.speed = 0f;

            goProfesor = goInicio.transform.Find("Profesor").gameObject;
            goGloboProfesor = goProfesor.transform.Find("GloboProfesor").gameObject;

            goLaboratorio = Canvas.transform.Find("Laboratorio").gameObject;
            goEnemigo = goLaboratorio.transform.Find("Enemigo").gameObject;
            goProfesorLast = goLaboratorio.transform.Find ("Profesor").gameObject;
            imgInterfaz = goEnemigo.transform.Find("imgInterfaz").GetComponent<Image>();

            goPociones = imgInterfaz.transform.Find("Pociones").gameObject;

            btnPocion1 = goPociones.transform.Find("btnPocion1").GetComponent<Button>();
            btnPocion1.onClick.AddListener(delegate
                {
                    MostrarReceta(1);
                    HabilitarZonaPreparacion();
                    contPocionServidas++;
                });
            btnPocion2 = goPociones.transform.Find("btnPocion2").GetComponent<Button>();
            btnPocion2.onClick.AddListener(delegate
                {
                    MostrarReceta(2);
                    HabilitarZonaPreparacion();
                    contPocionServidas++;
                });
            btnPocion3 = goPociones.transform.Find("btnPocion3").GetComponent<Button>();
            btnPocion3.onClick.AddListener(delegate
                {
                    MostrarReceta(3);
                    HabilitarZonaPreparacion();
                    contPocionServidas++;
                });
            btnPocion4 = goPociones.transform.Find("btnPocion4").GetComponent<Button>();
            btnPocion4.onClick.AddListener(delegate
                {
                    MostrarReceta(4);
                    HabilitarZonaPreparacion();
                    contPocionServidas++;
                });

            goReceta = imgInterfaz.transform.Find("Receta").gameObject;

            imgReceta = goReceta.transform.Find("imgReceta").GetComponent<Image>();
            txtReceta = imgReceta.transform.Find("txtReceta").GetComponent<Text>();

            goMezcla = imgInterfaz.transform.Find("Mezcla").gameObject;
            btnMezclar = goMezcla.transform.Find("btnMezclar").GetComponent<Button>();
            btnMezclar.onClick.AddListener(MezclarIngredientes);
            goAtaque = imgInterfaz.transform.Find("Ataque").gameObject;
            goMezclaAnimation = imgInterfaz.transform.Find("MezclaAnimation").gameObject;


            btnMezcla1 = goMezclaAnimation.transform.FindChild ("b1").GetComponent<Button> ();
            btnMezcla2 = goMezclaAnimation.transform.FindChild ("b2").GetComponent<Button> ();
            btnMezcla3 = goMezclaAnimation.transform.FindChild ("b3").GetComponent<Button> ();
            btnMezcla4 = goMezclaAnimation.transform.FindChild ("b4").GetComponent<Button> ();

            goZonaPreparacion = goEnemigo.transform.Find("ZonaPreparacion").gameObject;
            goIngredientes = goZonaPreparacion.transform.Find("Ingredientes").gameObject;

            btnLagrimas = goIngredientes.transform.Find("btnLagrimas").GetComponent<Button>();
            btnLagrimas.onClick.AddListener(delegate
                {
                    ZoomIn("Lagrimas");
                });

            btnAguaCactus = goIngredientes.transform.Find("btnAguaCactus").GetComponent<Button>();
            btnAguaCactus.onClick.AddListener(delegate
                {
                    ZoomIn("AguaCactus");
                });

            btnAguaManantial = goIngredientes.transform.Find("btnAguaManantial").GetComponent<Button>();
            btnAguaManantial.onClick.AddListener(delegate
                {
                    ZoomIn("AguaManantial");
                });

            btnAguaPozo = goIngredientes.transform.Find("btnAguaPozo").GetComponent<Button>();
            btnAguaPozo.onClick.AddListener(delegate
                {
                    ZoomIn("AguaPozo");
                });

            btnAguaTriangulo = goIngredientes.transform.Find("btnAguaTriangulo").GetComponent<Button>();
            btnAguaTriangulo.onClick.AddListener(delegate
                {
                    ZoomIn("AguaTriangulo");
                });

            btnAlasUnicornio = goIngredientes.transform.Find("btnAlasUnicornio").GetComponent<Button>();    
            btnAlasUnicornio.onClick.AddListener(delegate
                {
                    ZoomIn("AlasUnicornio");
                });

            btnAureola = goIngredientes.transform.Find("btnAureola").GetComponent<Button>();
            btnAureola.onClick.AddListener(delegate
                {
                    ZoomIn("Aureola");
                });

            btnCorteza = goIngredientes.transform.Find("btnCorteza").GetComponent<Button>();
            btnCorteza.onClick.AddListener(delegate
                {
                    ZoomIn("Corteza");
                });

            btnEspina = goIngredientes.transform.Find("btnEspina").GetComponent<Button>();
            btnEspina.onClick.AddListener(delegate
                {
                    ZoomIn("Espina");
                });

            btnLimadura = goIngredientes.transform.Find("btnLimadura").GetComponent<Button>();
            btnLimadura.onClick.AddListener(delegate
                {
                    ZoomIn("Limadura");
                });

            btnMaderaBarco = goIngredientes.transform.Find("btnMaderaBarco").GetComponent<Button>();
            btnMaderaBarco.onClick.AddListener(delegate
                {
                    ZoomIn("MaderaBarco");
                });

            btnPlumaAveztruz = goIngredientes.transform.Find("btnPlumaAveztruz").GetComponent<Button>();
            btnPlumaAveztruz.onClick.AddListener(delegate
                {
                    ZoomIn("PlumaAveztruz");
                });

            btnSalivaCaracol = goIngredientes.transform.Find("btnSalivaCaracol").GetComponent<Button>();
            btnSalivaCaracol.onClick.AddListener(delegate
                {
                    ZoomIn("SalivaCaracol");
                });

            btnSalivaTroll = goIngredientes.transform.Find("btnSalivaTroll").GetComponent<Button>();
            btnSalivaTroll.onClick.AddListener(delegate
                {
                    ZoomIn("SalivaTroll");
                });

            btnTierraTunel = goIngredientes.transform.Find("btnTierraTunel").GetComponent<Button>();
            btnTierraTunel.onClick.AddListener(delegate
                {
                    ZoomIn("TierraTunel");
                });

            btnUniaCorrecaminos = goIngredientes.transform.Find("btnUniaCorrecaminos").GetComponent<Button>();
            btnUniaCorrecaminos.onClick.AddListener(delegate
                {
                    ZoomIn("UniaCorrecaminos");
                });

            btnUniaLeon = goIngredientes.transform.Find("btnUniaLeon").GetComponent<Button>();
            btnUniaLeon.onClick.AddListener(delegate
                {
                    ZoomIn("UniaLeon");
                });

            foreach (string ingrediente in lsIngredientes)
            {
                Debug.Log(ingrediente);
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
            }

            goZonaBloqueo = goZonaPreparacion.transform.Find("ZonaBloqueo").gameObject;

            goMedidorLiquidos = goZonaPreparacion.transform.Find("MedidorLiquidos").gameObject;
            sldMedidorLiquidos = goMedidorLiquidos.transform.Find("imgProbeta").GetComponent<Image>().transform.Find("sldMedidor").GetComponent<Slider>();
            imgAreaInteractiva = sldMedidorLiquidos.GetComponentsInChildren<Image>().FirstOrDefault(item => item.name == "imgAreaInteractiva");
            btnAceptarLiquidos = goMedidorLiquidos.transform.Find("btnAceptar").GetComponent<Button>();
            btnAceptarLiquidos.onClick.AddListener(GuardarProporcion);

            goMedidorSolidos = goZonaPreparacion.transform.Find("MedidorSolidos").gameObject;
            txtProporcionSolido = goMedidorSolidos.transform.Find("imgBascula").GetComponent<Image>().transform.Find("txtBascula").GetComponent<Text>();
            btnAceptarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnAceptar").GetComponent<Button>();
            btnAceptarSolidos.onClick.AddListener(GuardarProporcion);
            btnBorrarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnBorrar").GetComponent<Button>();
            btnBorrarSolidos.onClick.AddListener(BorrarProporcion);
            btnTeclado0 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla0").GetComponent<Button>();
            btnTeclado0.onClick.AddListener(delegate
                {
                    SumarProporcion(0);
                });
            btnTeclado1 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla1").GetComponent<Button>();
            btnTeclado1.onClick.AddListener(delegate
                {
                    SumarProporcion(1);
                });
            btnTeclado2 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla2").GetComponent<Button>();
            btnTeclado2.onClick.AddListener(delegate
                {
                    SumarProporcion(2);
                });
            btnTeclado3 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla3").GetComponent<Button>();
            btnTeclado3.onClick.AddListener(delegate
                {
                    SumarProporcion(3);
                });
            btnTeclado4 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla4").GetComponent<Button>();
            btnTeclado4.onClick.AddListener(delegate
                {
                    SumarProporcion(4);
                });
            btnTeclado5 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla5").GetComponent<Button>();
            btnTeclado5.onClick.AddListener(delegate
                {
                    SumarProporcion(5);
                });
            btnTeclado6 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla6").GetComponent<Button>();
            btnTeclado6.onClick.AddListener(delegate
                {
                    SumarProporcion(6);
                });
            btnTeclado7 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla7").GetComponent<Button>();
            btnTeclado7.onClick.AddListener(delegate
                {
                    SumarProporcion(7);
                });
            btnTeclado8 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla8").GetComponent<Button>();
            btnTeclado8.onClick.AddListener(delegate
                {
                    SumarProporcion(8);
                });
            btnTeclado9 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla9").GetComponent<Button>();
            btnTeclado9.onClick.AddListener(delegate
                {
                    SumarProporcion(9);
                });

            InicializarPociones();
        }

        // Update is called once per frame
        protected override void Update()
        {
            // Initial state
            if (Instance.activeInHierarchy)
            {
                if (!esRetoActivo)
                {
                    GenericUi.ShowInfoPopUp(
                        "¡La escuela de magia Kadabra, se alegra de tenerte entre sus estudiantes! Deberás esforzarte porque el profesor Galaguer es uno de los más exigentes",
                        new ESequence {
                            Action = (Challenge.Action) Action.ShowProfessor,
                            ChallengeNum = 1,
                            Event = Event.ClosePopUp
                        }, 0.0f
                    );
                    esRetoActivo = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ingredienteActual) && !esZoomInTerminado &&
                        animZoom.GetCurrentAnimatorStateInfo(0).IsName(ingredienteActual) &&
                        animZoom.GetCurrentAnimatorStateInfo(0).normalizedTime % animZoom.GetCurrentAnimatorStateInfo(0).length  > 1.0f)
                        ZoomOut(ingredienteActual);
                }
            }
            else
                esRetoActivo = false;
            AnimateText();

        }

        private void CheckAttack(Button btn)
        {
            numAtaquesTotal++;
            if (btn.name.Contains("Buena"))
            {
                numAtaquesEnemigo++;
                goAtaque.transform.FindChild("EnviarAtaque").gameObject.SetActive(true);
                animVidaEnemigo = goAtaque.transform.FindChild("EnviarAtaque").FindChild("imgVidaGrifo").GetComponent<Animator>();
                animVidaEnemigo.Play("AnimationEnviarAtaque"+numAtaquesEnemigo);
                StartCoroutine(WaitStopAnimation(2.5f, animVidaEnemigo,true));
            }
            else
            {
                numAtaquesAlumno++;
                goAtaque.transform.FindChild ("Grifo").GetComponent<Animator> ().Play("AnimationGrifoAtaque");
                StartCoroutine (WaitAnimation (1.0f));
            }
            btn.enabled = false;
            btn.onClick.RemoveAllListeners ();
        }

        private IEnumerator WaitAnimation (float time){
            yield return new WaitForSeconds (time);
            goAtaque.transform.FindChild("RecibirAtaque").gameObject.SetActive(true);
            animVidaAlumno = goAtaque.transform.FindChild("imgVidaAlumno").GetComponent<Animator>();
            animVidaAlumno.Play("AnimationRecibirAtaque"+numAtaquesAlumno);
            animMezclaGeneric = goAtaque.transform.FindChild("RecibirAtaque").GetComponent<Animator>();
            animMezclaGeneric.Play ("AnimationRecibirAtaque");
            if (numAtaquesTotal < 4) {
                StartCoroutine (WaitStopAnimation (1.2f, animMezclaGeneric, false));
            } else {
                StartCoroutine (WaitStopAnimation (1.2f, animMezclaGeneric, true));
            }
        }

        private IEnumerator WaitStopAnimation(float timeWait,Animator anim,bool option)
        {
            yield return new WaitForSeconds(timeWait);
            if (!option) {
                goAtaque.transform.FindChild ("RecibirAtaque").gameObject.SetActive (false);
                goAtaque.transform.FindChild ("Grifo").GetComponent<Animator> ().Play("AnimationGrifo");
            } else {
                //test
                if (numAtaquesTotal >= 4) {
                    if (numAtaquesEnemigo >= 4) {//gano
                        goAtaque.transform.FindChild ("Grifo").GetComponent<Animator> ().Play ("AnimationGrifoCaida");
                        StartCoroutine (WaitAnimationFinal ());
                    } else {//perdio
                        goEnemigo.SetActive (false);
                        goProfesorLast.SetActive (true);
                        MostrarGloboProfesorLast ("¡He tenido que salvarte! Las pociones no tienen la cantidad de ingredientes adecuada. Vuelve a intentarlo y fíjate bien en cuál es la dosis necesaria para cada poción.", 10.0f, 2);
                    }
                }
            }
        }

        private IEnumerator WaitAnimationFinal(){
            yield return new WaitForSeconds (1.5f);
            goAtaque.transform.FindChild ("Grifo").GetComponent<Animator>().enabled=false;
            int estrellas = PlayerPrefs.GetInt("estrellasODA08_ODA08");
            estrellas++;
            if (estrellas > 3)
                estrellas = 1;
            Debug.Log ("estrellas :"+estrellas);
            PlayerPrefs.SetInt("estrellasODA08_ODA08",estrellas);
            GenericUi.PlayAnimationStar ();
            StartCoroutine(WaitStar ());
        }

        private IEnumerator  WaitStar()
        {
            yield return new WaitForSeconds(5.7f);
            Debug.Log ("popup");
            goEnemigo.SetActive(false);
            goProfesorLast.SetActive(true);
            MostrarGloboProfesorLast ("¡Bien hecho! Has superado tu primera prueba,  ¿tendrás el valor de enfrentar al siguiente monstruo?", 7.0f,1);
        }

        private void InicializarPociones()
        {
            //ReSharper disable once UseObjectOrCollectionInitializer
            Debug.Log(GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid));
            loRecetas = new List<ERecipe>();
            loRecetas.Add(new ERecipe
                {
                    Dosage = GetDosage(),
                    Mix = new Dictionary<string, EIngredient>
                    {
                        {"Corteza", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                        {"AguaCactus", new EIngredient {Color = new Color32(0x79, 0xD0, 0xD3, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"PlumaAveztruz", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"SalivaCaracol", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"Aureola", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}}
                    }
                });
            loRecetas.Add(new ERecipe
                {
                    Dosage = GetDosage(),
                    Mix = new Dictionary<string, EIngredient>
                    {
                        {"Limadura", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                        {"UniaLeon", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"AguaManantial", new EIngredient {Color = new Color32(0x04, 0xF0, 0xFF, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"SalivaTroll", new EIngredient {Color = new Color32(0x00, 0xED, 0x00, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}}
                    }
                });
            loRecetas.Add(new ERecipe
                {
                    Dosage = GetDosage(),
                    Mix = new Dictionary<string, EIngredient>
                    {
                        {"Espina", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                        {"Corteza", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"Lagrimas", new EIngredient {Color = new Color32(0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"AguaPozo", new EIngredient {Color = new Color32(0x2B, 0x3A, 0x3B, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"TierraTunel", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}}
                    }
                });
            loRecetas.Add(new ERecipe
                {
                    Dosage = GetDosage(),
                    Mix = new Dictionary<string, EIngredient>
                    {
                        {"UniaCorrecaminos", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                        {"AlasUnicornio", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                        {"AguaTriangulo", new EIngredient {Color = new Color32(0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"MaderaBarco", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}}
                    }
                });
        }

        private void ClosePopUp(Action action)
        {
            switch (action)
            {
            // Cases
            case Action.ShowEnemy:
                goEnemigo.SetActive(true);
                break;
            case Action.ShowProfessor:
                goProfesor.SetActive(true);
                animPuerta.Play("Puerta");
                animPuerta.speed = 1f;
                Invoke("MostrarGloboProfesor", 0.45f);
                break;
            case Action.EndChallenge:
                ReStart ();
                this.Active (false);
                goReto2.Active (true);
                break;
            }
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void MostrarGloboProfesor()
        {
            goGloboProfesor.SetActive(true);
            Texto = goGloboProfesor.transform.Find("txtGloboProfesor").GetComponent<Text>();
            SetTextToAnimate("Toca la puerta para iniciar el curso de Brebajes y pociones.", 4.0f);
            Invoke("EnableDoor",4.0f);
        }

        private void EnableDoor()
        {
            banPopUp = true;
        }


        private void MostrarLaboratorio()
        {
            if (!banPopUp) return;
            goLaboratorio.SetActive(true);
            goInicio.SetActive(false);
            GenericUi.ShowInfoPopUp(
                "Prueba 1: vencer al Grifo.\nSelecciona y prepara las pociones, indica los gramos o mililitros que corresponden a la dosis necesaria para que funcionen las pociones.",
                new ESequence
                {
                    Action = (Challenge.Action)Action.ShowEnemy,
                    ChallengeNum = 1,
                    Event = Event.ClosePopUp
                }, 0.0f
            );
        }

        private void MostrarReceta(int numPocion)
        {
            numPocionActual = numPocion;
            Debug.Log (loRecetas[0].Mix["Corteza"].Quantity);
            goPociones.SetActive(false);
            goReceta.SetActive(true);
            Debug.Log ("listas de ingredientes");
            switch (numPocion)
            {
            // Recetas
            case 1:
                txtReceta.text = 
                    "<size=24><b>APTERA:</b></size> Evita volar.\n\n" +
                    "Para <b>una</b> dosis:\n" +
                    "   " + loRecetas [0].Mix ["Corteza"].Quantity + " gr de corteza de árbol llorón\n"+
                    "   " + loRecetas[0].Mix["AguaCactus"].Quantity + " ml de agua de cactus\n" +
                    "   " + loRecetas[0].Mix["PlumaAveztruz"].Quantity + " gr de plumas de avestruz\n" +
                    "   " + loRecetas[0].Mix["SalivaCaracol"].Quantity + " ml de saliva de caracol\n" +
                    "   " + loRecetas[0].Mix["Aureola"].Quantity + " gr de aureola de ángel caído\n\n" +
                    "<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: " + loRecetas[0].Dosage + " DOSIS.</b>";
                break;
            case 2:
                txtReceta.text = 
                    "<size=24><b>UNGUIBUS:</b></size> Evita que use sus garras.\n\n" +
                    "Para <b>una</b> dosis:\n" +
                    "   " + loRecetas[1].Mix["Limadura"].Quantity + " gr de lija celestial\n" +
                        "   " + loRecetas[1].Mix["UniaLeon"].Quantity + " gr de uña molida de león\n" +
                    "   " + loRecetas[1].Mix["AguaManantial"].Quantity + " ml de agua del gran manantial\n" +
                    "   " + loRecetas[1].Mix["SalivaTroll"].Quantity + " ml de saliva ácida de troll\n\n" +
                    "<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: " + loRecetas[1].Dosage + " DOSIS.</b>";
                break;
            case 3:
                txtReceta.text = 
                    "<size=24><b>CAVAEM:</b></size> Encierra en una prisión invisible.\n\n" +
                    "Para <b>una</b> dosis:\n" +
                    "   " + loRecetas[2].Mix["Espina"].Quantity + " gr de espinas de puercoespín\n" +
                    "   " + loRecetas[2].Mix["Corteza"].Quantity + " gr de corteza de árbol llorón\n" +
                    "   " + loRecetas[2].Mix["Lagrimas"].Quantity + " ml de lágrimas del eterno prisionero\n" +
                    "   " + loRecetas[2].Mix["AguaPozo"].Quantity + " ml de agua del pozo oscuro\n" +
                    "   " + loRecetas[2].Mix["TierraTunel"].Quantity + " gr de tierra del túnel sin salida\n" +
                    "<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: " + loRecetas[2].Dosage + " DOSIS.</b>";
                break;
            case 4:
                txtReceta.text = 
                    "<size=24><b>TRANSPORT:</b></size> Desaparece al enemigo y lo hace reaparecer en el lugar que pienses.\n" +
                    "Para <b>una</b> dosis:\n" +
                    "    " + loRecetas[3].Mix["UniaCorrecaminos"].Quantity + " gr de uña molida de correcaminos\n" +
                       "   " + loRecetas[3].Mix["AlasUnicornio"].Quantity + " gr de alas de unicornio\n" +
                    "   " + loRecetas[3].Mix["AguaTriangulo"].Quantity + " ml de agua del triángulo de las Bermudas\n" +
                    "   " + loRecetas[3].Mix["MaderaBarco"].Quantity + " gr de madera del barco de Barba Negra\n" +
                    "<b>PARA QUE FUNCIONE EN EL GRIFO NECESITAS: " + loRecetas[3].Dosage + " DOSIS.</b>";
                break;
            }

            foreach (string ingrediente in loRecetas[numPocionActual - 1].Mix.Keys)
            {
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = true;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = true;
                Debug.Log("ingrediente " + ingrediente);
                Debug.Log("proporcion " + loRecetas[numPocionActual - 1].Mix[ingrediente].Proportion);
                Debug.Log("cantaidad " + loRecetas[numPocionActual - 1].Mix[ingrediente].Quantity);
            }

            //GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/click", 1);

        }

        private void HabilitarZonaPreparacion()
        {
            goZonaBloqueo.SetActive(false);
            GenericUi.Resume();
            GenericUi.ShowStart ();
        }

        private void SumarProporcion(int cantidad)
        {
            if (((cantidad == 0 && txtProporcionSolido.text != "0") || cantidad != 0) &&
                txtProporcionSolido.text.Length < 8)
            {
                proporcionSolido += cantidad;
                txtProporcionSolido.text = proporcionSolido;
                GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
            }
        }

        private void BorrarProporcion()
        {
            proporcionSolido = string.Empty;
            txtProporcionSolido.text = "0";
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void GuardarProporcion()
        {
            Debug.Log("ingrediente: " + ingredienteActual);
            if (loRecetas[numPocionActual - 1].Mix[ingredienteActual].Type == EIngredient.TypeIngredient.Liquid)
            {
                // Guardar proporción de ingrediente líquido
                if ((int)sldMedidorLiquidos.value > 0)
                {
                    loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = Mathf.RoundToInt(sldMedidorLiquidos.value);
                    Debug.Log("Valor de liquido: "+ Mathf.RoundToInt(sldMedidorLiquidos.value));
                    sldMedidorLiquidos.value = 0;
                    ContabilizarIngredienteServido();
                }
                goMedidorLiquidos.SetActive(false); 
            }
            else
            {
                // Guardar proporción de ingrediente sólido
                if (!string.IsNullOrEmpty(proporcionSolido))
                {
                    int proporcion = int.Parse(proporcionSolido);
                    if (proporcion != 0)
                    {
                        loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = proporcion;
                        ContabilizarIngredienteServido();
                    }
                    Debug.Log("porcion de solido: "+proporcion);
                    proporcionSolido = string.Empty;
                    txtProporcionSolido.text = "0";
                }
                goMedidorSolidos.SetActive(false);
            }
        }

        private void ContabilizarIngredienteServido()
        {
            goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
            goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
            if (++numIngredientesServidos == loRecetas [numPocionActual - 1].Mix.Count)
                MezclarIngredientes ();
            //goMezcla.SetActive(true);
        }

        private void MezclarIngredientes()
        {
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
            goReceta.SetActive(false);
            goMezcla.SetActive(false);
            goPociones.SetActive(true);
            Debug.Log("pocion actual: "+numPocionActual);
            goPociones.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().enabled = false;
            AnimationIngredient();
            ingredienteActual = string.Empty;
            numIngredientesServidos = 0;
            numPocionActual = 0;
            if (contPocionServidas >= 4)
            {
                StartCoroutine(WaitRealizarAtaque(2.2f));
            }
        }

        private void EnableRealizarAtaque()
        {
            goAtaque.SetActive(true);
            goZonaPreparacion.SetActive(false);
            contPocionServidas = 0;

            btnMezcla1.enabled = true;
            btnMezcla1.onClick.AddListener(delegate ()
                {
                    CheckAttack(btnMezcla1);
                    btnMezcla1.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla1.onClick.RemoveAllListeners();
                });

            btnMezcla2.enabled = true;
            btnMezcla2.onClick.AddListener(delegate ()
                {
                    CheckAttack(btnMezcla2);
                    btnMezcla2.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla2.onClick.RemoveAllListeners();
                });

            btnMezcla3.enabled = true;
            btnMezcla3.onClick.AddListener(delegate ()
                {
                    CheckAttack(btnMezcla3);
                    btnMezcla3.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla3.onClick.RemoveAllListeners();
                });

            btnMezcla4.enabled = true;
            btnMezcla4.onClick.AddListener(delegate ()
                {
                    CheckAttack(btnMezcla4);
                    btnMezcla4.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla4.onClick.RemoveAllListeners();
                });
        }

        private IEnumerator WaitRealizarAtaque(float timeWait)
        {
            yield return new WaitForSeconds(timeWait);
            List<Button> arrayButtonMezcla = new List<Button>(){btnMezcla1,btnMezcla2,btnMezcla3, btnMezcla4};
            int iBanMezcla = 0;
            foreach (Button btn in arrayButtonMezcla) {
                if (banMezclaAnimationStop [iBanMezcla])
                    btn.GetComponent<Animator> ().Play ("AnimationMala" + (iBanMezcla + 1) + "P");
                else {
                    btn.GetComponent<Animator> ().Play ("AnimationBien" + (iBanMezcla + 1) + "P");
                }
                iBanMezcla++;
            }
            EnableRealizarAtaque();
        }

        private void AnimationIngredient()
        {
            bool ban = false;
            foreach(var ingrediente in loRecetas[numPocionActual - 1].Mix)
            {
                if (ingrediente.Value.Type == EIngredient.TypeIngredient.Liquid)
                {
                    if (!(ingrediente.Value.Quantity*loRecetas[numPocionActual-1].Dosage >= ingrediente.Value.Proportion-2 && ingrediente.Value.Quantity * loRecetas[numPocionActual-1].Dosage <= ingrediente.Value.Proportion + 2))
                    {
                        ban = true;
                        break;
                    }
                }
                else
                {
                    if (!(ingrediente.Value.Quantity * loRecetas[numPocionActual-1].Dosage == ingrediente.Value.Proportion))
                    {
                        ban = true;
                        break;
                    }
                }
                if (ban) break;
            }

            Debug.Log("BAndera:"+ban);
            if (ban)
            {
                goMezclaAnimation.transform.Find("imgMezclaMala"+numPocionActual).gameObject.SetActive(true);
                animMezclaMala = goMezclaAnimation.transform.Find("imgMezclaMala" + numPocionActual).GetComponent<Animator>();
                animMezclaMala.Play("AnimacionMala" + numPocionActual);
            }
            else
            {
                goMezclaAnimation.transform.Find("imgMezclaBuena" + numPocionActual).gameObject.SetActive(true);
                animMezclaBuena = goMezclaAnimation.transform.Find("imgMezclaBuena" + numPocionActual).GetComponent<Animator>();
                animMezclaBuena.Play("AnimacionBien" + numPocionActual);
            }
            goPociones.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().enabled = false;
            switch (numPocionActual)
            {
            case 1:
                if (ban) {
                    btnMezcla1 = goMezclaAnimation.transform.Find ("imgMezclaMala1").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla1 = goMezclaAnimation.transform.Find ("imgMezclaBuena1").GetComponent<Button> ();
                }
                btnMezcla1.enabled = false;
                break;
            case 2:
                if (ban) {
                    btnMezcla2 = goMezclaAnimation.transform.Find ("imgMezclaMala2").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla2 = goMezclaAnimation.transform.Find ("imgMezclaBuena2").GetComponent<Button> ();
                }
                btnMezcla2.enabled = false;
                break;
            case 3:
                if (ban) {
                    btnMezcla3 = goMezclaAnimation.transform.Find ("imgMezclaMala3").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla3 = goMezclaAnimation.transform.Find ("imgMezclaBuena3").GetComponent<Button> ();
                }
                btnMezcla3.enabled = false;
                break;
            case 4:
                if (ban) {
                    btnMezcla4 = goMezclaAnimation.transform.Find ("imgMezclaMala4").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla4 = goMezclaAnimation.transform.Find ("imgMezclaBuena4").GetComponent<Button> ();
                }
                btnMezcla4.enabled = false;
                break;
            }
        }

        private void ZoomIn(string ingrediente)
        {
            if (numPocionActual > 0 && loRecetas[numPocionActual - 1].Mix.ContainsKey(ingrediente))
            {
                esZoomInTerminado = false;
                ingredienteActual = ingrediente;
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = true;
                animZoom = goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>();
                animZoom.Play(ingrediente, -1, 0f);
                animZoom.speed = 1f;
                GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/zoom", 1);
            }
        }

        private void ZoomOut(string ingrediente)
        {
            esZoomInTerminado = true;
            if (loRecetas[numPocionActual - 1].Mix[ingrediente].Type == EIngredient.TypeIngredient.Liquid)
            {
                imgAreaInteractiva.color = loRecetas[numPocionActual - 1].Mix[ingrediente].Color;
                goMedidorLiquidos.SetActive(true);
            }
            else
                goMedidorSolidos.SetActive(true);
        }


        private void MostrarGloboProfesorLast(string msj,float timeMsj, int option)
        {
            Texto = goProfesorLast.transform.Find("GloboProfesor").Find("txtGloboProfesor").GetComponent<Text>();
            SetTextToAnimate(msj, timeMsj);
            switch (option) {
            case 0:
                break;
            case 1:
                Invoke ("ShowLastPopUp", 6.0f);
                break;
            case 2:
                Invoke ("backToMenu", timeMsj+2.2f);
                break;
            }
        }

        private void ShowLastPopUp(){
            GenericUi.ShowInfoPopUp(
                "¡Sigamos con el segundo reto!",
                new ESequence {
                    Action = (Challenge.Action) Action.EndChallenge,
                    ChallengeNum = 1,
                    Event = Event.ClosePopUp
                }, 0.0f
            );
        }

        private void backToMenu(){
            goMenu.Mostrar ();
        }

        // Protected métodos propios de la oda que no entiendo bien como funcionan
        /// <summary>
        /// Execute a serie of action/event of the sequence, after an specified time
        /// </summary>
        /// <param name="sequence">The action/event to execute</param>
        /// <param name="waitingTime">Waiting time to launch the action/event of the sequence</param>
        /// <returns></returns>
        internal override IEnumerator Execute(ESequence sequence, float waitingTime)
        {
            yield return new WaitForSeconds(waitingTime);
            switch (sequence.Event)
            {
            case Event.ClosePopUp:
                ClosePopUp((Action)sequence.Action);
                break;
            }
        }
        /// <summary>
        /// Set the challange to its initial state
        /// </summary>
        protected override void ReStart()
        {
            // Reset objects to initial state
            foreach (string ingrediente in lsIngredientes)
            {
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
            }
            goPociones.transform.Find("btnPocion1").GetComponent<Button>().enabled = true;
            goPociones.transform.Find("btnPocion2").GetComponent<Button>().enabled = true;
            goPociones.transform.Find("btnPocion3").GetComponent<Button>().enabled = true;
            goPociones.transform.Find("btnPocion4").GetComponent<Button>().enabled = true;
            txtProporcionSolido.text = "0";
            txtReceta.text = string.Empty;
            goMedidorLiquidos.SetActive(false);
            goMedidorSolidos.SetActive(false);
            goZonaBloqueo.SetActive(true);
            goMezcla.SetActive(false);
            goReceta.SetActive(false);
            goPociones.SetActive(true);
            goEnemigo.SetActive(false);
            goLaboratorio.SetActive(false);
            goGloboProfesor.SetActive(false);
            goProfesor.SetActive(false);
            goInicio.SetActive(true);
            animPuerta.speed = 0f;
            esRetoActivo = false;
            esZoomInTerminado = false;
            sldMedidorLiquidos.value = 0;
            numIngredientesServidos = 0;
            numPocionActual = 0; for (int i = 0; i < 4; i++)
            {
                banMezclaAnimationStop[i] = false;
            }
            ingredienteActual = string.Empty;
            proporcionSolido = string.Empty;

            contPocionServidas = 0;
            numAtaquesEnemigo = 0;
            numAtaquesAlumno = 0;
            numAtaquesTotal = 0;
            banPopUp = false;
            for (int i = 0; i < 4; i++)
            {
                banMezclaAnimationStop[i] = false;
            }

            goProfesorLast.SetActive (false);
            goAtaque.SetActive (false);
            goZonaPreparacion.SetActive (true);

            for (int i=1; i<5; i++)
                goMezclaAnimation.transform.FindChild ("imgMezclaBuena"+i).gameObject.SetActive(false);

            for (int i=1; i<5; i++)
                goMezclaAnimation.transform.FindChild ("imgMezclaMala"+i).gameObject.SetActive(false);

            goAtaque.transform.FindChild("RecibirAtaque").GetComponent<Animator>().Stop();
            goAtaque.transform.FindChild ("imgVidaAlumno").GetComponent<Animator> ().Play ("AnimationDefault");
            goAtaque.transform.FindChild("EnviarAtaque").FindChild("imgVidaGrifo").GetComponent<Animator>().Play("AnimationDefaultEnemigo");
            btnMezcla1.onClick.RemoveAllListeners ();
            btnMezcla2.onClick.RemoveAllListeners ();
            btnMezcla3.onClick.RemoveAllListeners ();
            btnMezcla4.onClick.RemoveAllListeners ();

            InicializarPociones();
        }

        /// <summary>
        /// Set the state of the sequence
        /// </summary>
        /// <param name="state">The new state of the sequence</param>
        internal  override void SetState(State state)
        {
            switch (state)
            {
            case State.Initial:
                ReStart();
                break;
            case State.Playing:
                break;
            case State.Finished:
                break;
            }
        }

        public int GenerarCantidadIngrediente(EIngredient.TypeIngredient tipoIngrediente)
        {
            int cantidad = 0;
            switch (tipoIngrediente)
            {
            case EIngredient.TypeIngredient.Liquid:
                cantidad = rndLiquidos.Next(5, 26);
                break;
            case EIngredient.TypeIngredient.Solid:
                cantidad = rndSolidos.Next(5, 251);
                break;
            }
            Debug.Log ("cantidad: "+(cantidad - cantidad % 5) );
            return cantidad - cantidad % 5;
        }

        public int GetDosage(){
            rndDosis = new System.Random();
            return 1;//rndDosis.Next (1, 5);
        }
    }
}