﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Oda08Reto3 : Challenge
    {
        //region Enumeration

        public new enum Action
        {
            ShowEnemy, ShowProfessor, EndChallenge
        }

        //endregion

        //region Attributes

        private Animator animPuerta, animZoom, animMezclaBuena, animMezclaMala, animMezclaGeneric, animVidaEnemigo, animVidaAlumno;
        private bool esRetoActivo, esZoomInTerminado, banPopUp;
        private Button btnPuerta, btnPocion1, btnPocion2, btnPocion3, btnPocion4, btnCorteza, btnAguaCactus, btnPlumaDeAvestruz, btnSalivaCaracol, btnAureola, btnCenizasDeFenix, btnRocaLunar, btnRocio, btnSalivaDeOgro, btnUnaDeYeti, btnRioQueCanta, btnPoloNorte, btnHadaNegra, btnCapaMerlin,
        btnEsenciaDeClavel, btnCuernoUnicornio, btnSalivaPieGrande, btnAceptarLiquidos, btnAceptarSolidos, btnBorrarSolidos, btnTeclado0, btnTeclado1, btnTeclado2, btnTeclado3, btnTeclado4, btnTeclado5, btnTeclado6, btnTeclado7, btnTeclado8, btnTeclado9;
        private Button btnMezcla1, btnMezcla2, btnMezcla3, btnMezcla4;
        private UnityEngine.GameObject goInicio, goProfesor, goGloboProfesor, goProfesorLast, goLaboratorio, goEnemigo, goPociones, goReceta, goMezcla, goAtaque, goMezclaAnimation, goZonaPreparacion, goIngredientes, goZonaBloqueo, goMedidorLiquidos, goMedidorSolidos;
        private Image imgInterfaz, imgReceta, imgAreaInteractiva;
        private int numPocionActual, numIngredientesServidos, contPocionServidas, numAtaquesEnemigo, numAtaquesAlumno, numAtaquesTotal, estrellas;
        private List<string> lsIngredientes;
        private List<ERecipe> loRecetas;
        private Slider sldMedidorLiquidos;
        private string ingredienteActual, proporcionSolido;
        private Text txtReceta, txtProporcionSolido;
        private Button btnMezclar;
        private Oda08Indice goMenu;
        private RetoFinalOda08 goRetoFinal;
        private bool[] banMezclaAnimationStop;
        public System.Random rndDosis,rndLiquidos, rndSolidos;


        public Oda08Reto3() : base("Reto3")
		{
			
		}

		// Use this for initialization
		protected override void Start()
        {
            //region Object instances

            lsIngredientes = new List<string> {
                "Aureola", "CapaDeMerlin","CenizasDeFenix","AguaCactus","PlumaDeAvestruz",
                "Corteza", "CuernoUnicornio", "EsenciaDeClavel","HadaNegra","PoloNorte",
                "PolvoCometa", "RioQueCanta","RocaLunar","Rocio","SalivaCaracol", 
                "SalivaDeOgro", "SalivaPieGrande", "UnaDeYeti" 
            };
            contPocionServidas = 0;
            numAtaquesEnemigo = 0;
            numAtaquesAlumno = 0;
            numAtaquesTotal = 0;
            banPopUp = false;
            banMezclaAnimationStop = new bool[4];
            for (int i = 0; i < 4; i++) {
                banMezclaAnimationStop [i] = false;
            }
            rndLiquidos = new System.Random();
            rndSolidos = new System.Random();
            rndDosis = new System.Random ();



            goMenu = transform.GetComponent<Oda08Indice>();
            goRetoFinal = transform.GetComponent<RetoFinalOda08>();

            goInicio = Canvas.transform.Find("Inicio").gameObject;
            goLaboratorio = Canvas.transform.Find("Laboratorio").gameObject;

            btnPuerta = goInicio.transform.Find("btnPuerta").GetComponent<Button>();
            goProfesor = goInicio.transform.Find("Profesor").gameObject;

            goEnemigo = goLaboratorio.transform.Find("Enemigo").gameObject;
            goProfesorLast = goLaboratorio.transform.Find("Profesor").gameObject;
            animPuerta = btnPuerta.GetComponent<Animator>();
            animPuerta.speed = 0f;

            goGloboProfesor = goProfesor.transform.Find("GloboProfesor").gameObject;

            imgInterfaz = goEnemigo.transform.Find("imgInterfaz").GetComponent<Image>();
            goZonaPreparacion = goEnemigo.transform.Find("ZonaPreparacion").gameObject;
            goIngredientes = goZonaPreparacion.transform.Find("Ingredientes").gameObject;

            goPociones = imgInterfaz.transform.Find("Pociones").gameObject;
            goReceta = imgInterfaz.transform.Find("Receta").gameObject;
            btnPocion1 = goPociones.transform.Find("btnPocion1").GetComponent<Button>();
            btnPocion2 = goPociones.transform.Find("btnPocion2").GetComponent<Button>();
            btnPocion3 = goPociones.transform.Find("btnPocion3").GetComponent<Button>();
            btnPocion4 = goPociones.transform.Find("btnPocion4").GetComponent<Button>();

            goMezcla = imgInterfaz.transform.Find("Mezcla").gameObject;
            btnMezclar = goMezcla.transform.Find("btnMezclar").GetComponent<Button>();

            goAtaque = imgInterfaz.transform.FindChild("Ataque").gameObject;
            goMezclaAnimation = imgInterfaz.transform.Find("MezclaAnimation").gameObject;
            btnMezcla1 = goMezclaAnimation.transform.FindChild ("b1").GetComponent<Button> ();
            btnMezcla2 = goMezclaAnimation.transform.FindChild ("b2").GetComponent<Button> ();
            btnMezcla3 = goMezclaAnimation.transform.FindChild ("b3").GetComponent<Button> ();
            btnMezcla4 = goMezclaAnimation.transform.FindChild ("b4").GetComponent<Button> ();

            imgReceta = goReceta.transform.Find("imgReceta").GetComponent<Image>();

            txtReceta = imgReceta.transform.Find("txtReceta").GetComponent<Text>();
            goZonaBloqueo = goZonaPreparacion.transform.Find("ZonaBloqueo").gameObject;

            btnCorteza = goIngredientes.transform.Find("btnCorteza").GetComponent<Button>();
            btnAguaCactus = goIngredientes.transform.Find("btnAguaCactus").GetComponent<Button>();
            btnPlumaDeAvestruz = goIngredientes.transform.Find("btnPlumaDeAvestruz").GetComponent<Button>();
            btnSalivaCaracol = goIngredientes.transform.Find("btnSalivaCaracol").GetComponent<Button>();
            btnAureola = goIngredientes.transform.Find("btnAureola").GetComponent<Button>();
            btnCenizasDeFenix = goIngredientes.transform.Find("btnCenizasDeFenix").GetComponent<Button>();
            btnRocaLunar =goIngredientes.transform.Find("btnRocaLunar").GetComponent<Button>();
            btnRocio =goIngredientes.transform.Find("btnRocio").GetComponent<Button>();
            btnSalivaDeOgro = goIngredientes.transform.Find("btnSalivaDeOgro").GetComponent<Button>();
            btnUnaDeYeti = goIngredientes.transform.Find("btnUnaDeYeti").GetComponent<Button>();
            btnRioQueCanta = goIngredientes.transform.Find("btnRioQueCanta").GetComponent<Button>();
            btnPoloNorte = goIngredientes.transform.Find("btnPoloNorte").GetComponent<Button>();
            btnHadaNegra = goIngredientes.transform.Find("btnHadaNegra").GetComponent<Button>();
            btnCapaMerlin = goIngredientes.transform.Find("btnCapaDeMerlin").GetComponent<Button>();
            btnEsenciaDeClavel = goIngredientes.transform.Find("btnEsenciaDeClavel").GetComponent<Button>();
            btnCuernoUnicornio = goIngredientes.transform.Find("btnCuernoUnicornio").GetComponent<Button>();
            btnSalivaPieGrande = goIngredientes.transform.Find("btnSalivaPieGrande").GetComponent<Button>();

            foreach (string ingrediente in lsIngredientes)
            {
                Debug.Log("img" + ingrediente);
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
            }

            goMedidorLiquidos = goZonaPreparacion.transform.Find("MedidorLiquidos").gameObject;
            goMedidorSolidos = goZonaPreparacion.transform.Find("MedidorSolidos").gameObject;

            btnAceptarLiquidos = goMedidorLiquidos.transform.Find("btnAceptar").GetComponent<Button>();
            sldMedidorLiquidos = goMedidorLiquidos.transform.Find("imgProbeta").GetComponent<Image>().transform.Find("sldMedidor").GetComponent<Slider>();

            imgAreaInteractiva = sldMedidorLiquidos.GetComponentsInChildren<Image>().FirstOrDefault(item => item.name == "imgAreaInteractiva");

            txtProporcionSolido = goMedidorSolidos.transform.Find("imgBascula").GetComponent<Image>().transform.Find("txtBascula").GetComponent<Text>();
            btnAceptarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnAceptar").GetComponent<Button>();
            btnBorrarSolidos = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnBorrar").GetComponent<Button>();
            btnTeclado0 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla0").GetComponent<Button>();
            btnTeclado1 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla1").GetComponent<Button>();
            btnTeclado2 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla2").GetComponent<Button>();
            btnTeclado3 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla3").GetComponent<Button>();
            btnTeclado4 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla4").GetComponent<Button>();
            btnTeclado5 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla5").GetComponent<Button>();
            btnTeclado6 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla6").GetComponent<Button>();
            btnTeclado7 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla7").GetComponent<Button>();
            btnTeclado8 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla8").GetComponent<Button>();
            btnTeclado9 = goMedidorSolidos.transform.Find("imgTeclado").GetComponent<Image>().transform.Find("btnTecla9").GetComponent<Button>();

            InicializarPociones();

            //endregion

            //region Event listeners

            btnPuerta.onClick.AddListener(MostrarLaboratorio);
            btnPocion1.onClick.AddListener(delegate
            {
                MostrarReceta(1);
                HabilitarZonaPreparacion();
                contPocionServidas++;
            });
            btnPocion2.onClick.AddListener(delegate
            {
                MostrarReceta(2);
                HabilitarZonaPreparacion();
                contPocionServidas++;
            });
            btnPocion3.onClick.AddListener(delegate
            {
                MostrarReceta(3);
                HabilitarZonaPreparacion();
                contPocionServidas++;
            });
            btnPocion4.onClick.AddListener(delegate
            {
                MostrarReceta(4);
                HabilitarZonaPreparacion();
                contPocionServidas++;
            });
            btnMezclar.onClick.AddListener(MezclarIngredientes);


            btnCorteza.onClick.AddListener(delegate
            {
                ZoomIn("Corteza");
            });
            btnAguaCactus.onClick.AddListener(delegate
            {
                ZoomIn("AguaCactus");
            });
            btnPlumaDeAvestruz.onClick.AddListener(delegate
            {
                ZoomIn("PlumaDeAvestruz");
            });
            btnSalivaCaracol.onClick.AddListener(delegate
            {
                ZoomIn("SalivaCaracol");
            });
            btnAureola.onClick.AddListener(delegate
            {
                ZoomIn("Aureola");
            });
            btnCenizasDeFenix.onClick.AddListener(delegate
            {
                ZoomIn("CenizasDeFenix");
            });
            btnRocaLunar.onClick.AddListener(delegate
            {
                ZoomIn("RocaLunar");
            });
            btnRocio.onClick.AddListener(delegate
            {
                ZoomIn("Rocio");
            });
            btnSalivaDeOgro.onClick.AddListener(delegate
            {
                ZoomIn("SalivaDeOgro");
            });
            btnUnaDeYeti.onClick.AddListener(delegate
            {
                ZoomIn("UnaDeYeti");
            });
            btnRioQueCanta.onClick.AddListener(delegate
            {
                ZoomIn("RioQueCanta");
            });
            btnPoloNorte.onClick.AddListener(delegate
            {
                ZoomIn("PoloNorte");
            });
            btnHadaNegra.onClick.AddListener(delegate
            {
                ZoomIn("HadaNegra");
            });
            btnCapaMerlin.onClick.AddListener(delegate
            {
                ZoomIn("CapaDeMerlin");
            });
            btnEsenciaDeClavel.onClick.AddListener(delegate
            {
                ZoomIn("EsenciaDeClavel");
            });
            btnCuernoUnicornio.onClick.AddListener(delegate
            {
                ZoomIn("CuernoUnicornio");
            });
            btnSalivaPieGrande.onClick.AddListener(delegate
            {
                ZoomIn("SalivaPieGrande");
            });


            btnAceptarLiquidos.onClick.AddListener(GuardarProporcion);
            btnAceptarSolidos.onClick.AddListener(GuardarProporcion);
            btnBorrarSolidos.onClick.AddListener(BorrarProporcion);
            btnTeclado0.onClick.AddListener(delegate
            {
                SumarProporcion(0);
            });
            btnTeclado1.onClick.AddListener(delegate
            {
                SumarProporcion(1);
            });
            btnTeclado2.onClick.AddListener(delegate
            {
                SumarProporcion(2);
            });
            btnTeclado3.onClick.AddListener(delegate
            {
                SumarProporcion(3);
            });
            btnTeclado4.onClick.AddListener(delegate
            {
                SumarProporcion(4);
            });
            btnTeclado5.onClick.AddListener(delegate
            {
                SumarProporcion(5);
            });
            btnTeclado6.onClick.AddListener(delegate
            {
                SumarProporcion(6);
            });
            btnTeclado7.onClick.AddListener(delegate
            {
                SumarProporcion(7);
            });
            btnTeclado8.onClick.AddListener(delegate
            {
                SumarProporcion(8);
            });
            btnTeclado9.onClick.AddListener(delegate
            {
                SumarProporcion(9);
            });

            //endregion
        }

        // Update is called once per frame
        protected override void Update()
        {
            //region Initial state

            if (Instance.activeInHierarchy)
            {
                if (!esRetoActivo)
                {
                    GenericUi.ShowInfoPopUp(
                        "Demuéstrale al profesor Galaguer que eres el mejor aprendiz mago y que tus pociones son infalibles. ",
                        new ESequence
                        {
                            Action = (Challenge.Action)Action.ShowProfessor,
                            ChallengeNum = 3,
                            Event = Event.ClosePopUp
                        }, 0.0f
                    );
                    esRetoActivo = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ingredienteActual) && !esZoomInTerminado &&
                        animZoom.GetCurrentAnimatorStateInfo(0).IsName("Animationimg"+  ingredienteActual) &&
                        animZoom.GetCurrentAnimatorStateInfo(0).normalizedTime % animZoom.GetCurrentAnimatorStateInfo(0).length > 1.0f)
                        ZoomOut(ingredienteActual);
                }
            }
            else
                esRetoActivo = false;
            AnimateText();
        }

        private void InicializarPociones()
        {
            //ReSharper disable once UseObjectOrCollectionInitializer
            loRecetas = new List<ERecipe>();

            //region Initializing potion mix

            loRecetas.Add(new ERecipe
            {
                    Dosage = GetDosage(),
                Mix = new Dictionary<string, EIngredient>
                {
                        {"Corteza", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
             /*           {"AguaCactus", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"PlumaDeAvestruz", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"SalivaCaracol", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"Aureola", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}}*/
                }

            });
            loRecetas.Add(new ERecipe
            {
                    Dosage = GetDosage(),
                Mix = new Dictionary<string, EIngredient>
                {
                        {"CenizasDeFenix", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                        /*{"RocaLunar", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"Rocio", new EIngredient {Color = new Color32 (0x04, 0xF0, 0xFF, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"SalivaDeOgro", new EIngredient {Color = new Color32 (0x00, 0xED, 0x00, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}}*/
                }

            });
            loRecetas.Add(new ERecipe
            {
                    Dosage = GetDosage(),
                Mix = new Dictionary<string, EIngredient>
                {
                        {"UnaDeYeti", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                        /*{"RioQueCanta", new EIngredient {Color = new Color32 (0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"PoloNorte", new EIngredient {Color = new Color32 (0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"RocaLunar", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},*/
                    }
            });
            loRecetas.Add(new ERecipe
            {
                    Dosage = GetDosage(),
                Mix = new Dictionary<string, EIngredient>
                {
                        {"HadaNegra", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                        /*{"CapaDeMerlin", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"EsenciaDeClavel", new EIngredient {Color = new Color32 (0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"CuernoUnicornio", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"SalivaPieGrande", new EIngredient {Color = new Color32 (0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},*/
                }

            });

            //endregion
        }

        private void ClosePopUp(Action action)
        {
            switch (action)
            {
                //region Cases

                case Action.ShowEnemy:
                    goEnemigo.SetActive(true);
                    break;
                case Action.ShowProfessor:
                    goProfesor.SetActive(true);
                    animPuerta.Play("Puerta");
                    animPuerta.speed = 1f;
                    Invoke("MostrarGloboProfesor", 0.45f);
                    break;
            case Action.EndChallenge:
                ReStart ();
                this.Active (false);
                goRetoFinal.Active (true);
                PlayerPrefs.SetInt ("estrellasODA08_ODA08",0);
                break;
                    //endregion
            }

            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void MostrarGloboProfesor()
        {
            goGloboProfesor.SetActive(true);
            Texto = goGloboProfesor.transform.Find("txtGloboProfesor").GetComponent<Text>();
            SetTextToAnimate("Toca la puerta e impresióname con tus brebajes.", 4.0f);
            Invoke("EnableDoor", 4.0f);
        }

        private void EnableDoor()
        {
            banPopUp = true;
        }

        private void MostrarLaboratorio()
        {
            if (!banPopUp) return;
            goLaboratorio.SetActive(true);
            goInicio.SetActive(false);
            string msjPopUP = "";
            
            int numEstrellas=PlayerPrefs.GetInt("estrellasODA08_ODA08");

            if (numEstrellas == 0) {
                msjPopUP = "Prueba 3: vencer al Dragón.\n Selecciona y prepara las pociones, indica los gramos o mililitros que corresponden a la dosis necesaria para que funcionen las pociones.";
            }else if(numEstrellas == 1) {
                msjPopUP = "Prueba 3: vencer al Dragón.\n Gracias a tu estrella tienes 1 minuto extra para preparar las pociones.";
                //cambiar tiempo
                GenericUi.Time=359;
            }else if (numEstrellas >= 2) {
                msjPopUP = "Prueba 3: vencer al Dragón.\n Gracias a tus estrellas tienes 2 minutos extras para preparar las pociones.";
                //cambiar tiempo
                GenericUi.Time=419;
            }
            GenericUi.ShowInfoPopUp(
                msjPopUP,
                new ESequence
                {
                    Action = (Challenge.Action)Action.ShowEnemy,
                    ChallengeNum = 3,
                    Event = Event.ClosePopUp
                }, 0.0f
            );
        }

        private void MostrarReceta(int numPocion)
        {
            numPocionActual = numPocion;
            goPociones.SetActive(false);
            goReceta.SetActive(true);

            switch (numPocion)
            {
                //region Recetas

                case 1:
                    txtReceta.text =
                        "<size=24><b>APTERA:</b></size> Evita volar.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                    "	"+loRecetas[numPocion-1].Mix["Corteza"].Quantity+" gr de corteza de árbol llorón \n" +
                    /*"	"+loRecetas[numPocion-1].Mix["AguaCactus"].Quantity+" ml de agua de cactus\n" +
                    "	"+loRecetas[numPocion-1].Mix["PlumaDeAvestruz"].Quantity+" gr de plumas de avestruz \n" +
                    "	"+loRecetas[numPocion-1].Mix["SalivaCaracol"].Quantity+" ml de saliva de caracol\n" +
                    "	"+loRecetas[numPocion-1].Mix["Aureola"].Quantity+"gr de aureola de ángel caído\n\n" +*/
                    "<b>PARA QUE FUNCIONE EN EL DRAGÓN NECESITAS: "+loRecetas[numPocion-1].Dosage+" DOSIS.</b>";
                    break;

                case 2:
                    txtReceta.text =
                        "<size=24><b>AVERTE:</b></size> : Apaga las llamas que puede lanzar.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                    "   "+loRecetas[numPocion-1].Mix["CenizasDeFenix"].Quantity+" gr de ceniza del fénix\n" +
                    /*"	"+loRecetas[numPocion-1].Mix["RocaLunar"].Quantity+" gr de roca lunar\n" +
                    "	"+loRecetas[numPocion-1].Mix["Rocio"].Quantity+" ml de rocío\n" +
                    "	"+loRecetas[numPocion-1].Mix["SalivaDeOgro"].Quantity+" ml de saliva de un ogro\n\n" +*/
                    "<b>PARA QUE FUNCIONE EN EL DRAGÓN NECESITAS: "+loRecetas[numPocion-1].Dosage+" DOSIS.</b>";
                    break;
                case 3:
                    txtReceta.text =
                        "<size=24><b> DURATUS:</b></size> Enfría y evita que se mueva.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                    "	"+loRecetas[numPocion-1].Mix["UnaDeYeti"].Quantity+" gr de uña molida del Yeti\n" +
                    /*"	"+loRecetas[numPocion-1].Mix["RioQueCanta"].Quantity+" ml de agua de río que canta\n" +
                    "	"+loRecetas[numPocion-1].Mix["PoloNorte"].Quantity+" ml de agua helada del polo norte\n" +
                    "	"+loRecetas[numPocion-1].Mix["RocaLunar"].Quantity+" gr de roca lunar \n\n" +*/
                    "<b>PARA QUE FUNCIONE EN EL DRAGÓN NECESITAS: "+loRecetas[numPocion-1].Dosage+" DOSIS.</b>";
                    break;
                case 4:
                    txtReceta.text =
                        "<size=24><b> DEPELLENDAM:</b></size> Desvanece al enemigo cuando se encuentra débil.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                    "	"+loRecetas[numPocion-1].Mix["HadaNegra"].Quantity+" gr de polvo de Hada Negra\n" +
                    /*"	"+loRecetas[numPocion-1].Mix["CapaDeMerlin"].Quantity+" gr de tela de la capa de Merlín\n" +
                    "	"+loRecetas[numPocion-1].Mix["EsenciaDeClavel"].Quantity+" ml de esencia de clavel\n" +
                    "	"+loRecetas[numPocion-1].Mix["CuernoUnicornio"].Quantity+" gr de cuerno de Unicornio molido\n" +
                    "	"+loRecetas[numPocion-1].Mix["SalivaPieGrande"].Quantity+" ml de saliva de Pie Grande\n\n" +*/

                    "<b>PARA QUE FUNCIONE EN EL DRAGÓN NECESITAS: "+loRecetas[numPocion-1].Dosage+" DOSIS.</b>";
                    break;
            }

            foreach (string ingrediente in loRecetas[numPocionActual - 1].Mix.Keys)
            {
                Debug.Log("btn---" + ingrediente);
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = true;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = true;
            }

            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void HabilitarZonaPreparacion()
        {
            goZonaBloqueo.SetActive(false);
            GenericUi.Resume();
            GenericUi.ShowStart ();
        }

        private void SumarProporcion(int cantidad)
        {
            if (((cantidad == 0 && txtProporcionSolido.text != "0") || cantidad != 0) &&
                txtProporcionSolido.text.Length < 8)
            {
                proporcionSolido += cantidad;
                txtProporcionSolido.text = proporcionSolido;
                GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
            }
        }

        private void BorrarProporcion()
        {
            proporcionSolido = string.Empty;
            txtProporcionSolido.text = "0";
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void GuardarProporcion()
        {
            if (loRecetas[numPocionActual - 1].Mix[ingredienteActual].Type == EIngredient.TypeIngredient.Liquid)
            {
                //region Guardar proporción de ingrediente líquido

                if ((int)sldMedidorLiquidos.value > 0)
                {
                    loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = Mathf.RoundToInt(sldMedidorLiquidos.value);
                    sldMedidorLiquidos.value = 0;
                    ContabilizarIngredienteServido();
                }

                goMedidorLiquidos.SetActive(false);

                //endregion
            }
            else
            {
                //region Guardar proporción de ingrediente sólido

                if (!string.IsNullOrEmpty(proporcionSolido))
                {
                    int proporcion = int.Parse(proporcionSolido);

                    if (proporcion != 0)
                    {
                        loRecetas[numPocionActual - 1].Mix[ingredienteActual].Proportion = proporcion;
                        ContabilizarIngredienteServido();
                    }

                    proporcionSolido = string.Empty;
                    txtProporcionSolido.text = "0";
                }

                goMedidorSolidos.SetActive(false);

                //endregion
            }
        }

        private void ContabilizarIngredienteServido()
        {
            goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
            goIngredientes.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);

            if (++numIngredientesServidos == loRecetas [numPocionActual - 1].Mix.Count)
                MezclarIngredientes ();
        }
        
        private void MezclarIngredientes()
        {
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);

            goMezcla.SetActive(false);
            goReceta.SetActive(false);
            goPociones.SetActive(true);

            goPociones.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().enabled = false;
            AnimationIngredient();
            ingredienteActual = string.Empty;
            numIngredientesServidos = 0;
            numPocionActual = 0;
            if (contPocionServidas >= 4)
            {
                StartCoroutine(WaitRealizarAtaque(2.2f));
            }
        }
        private IEnumerator WaitRealizarAtaque(float timeWait)
        {
            yield return new WaitForSeconds(timeWait);

            List<Button> arrayButtonMezcla = new List<Button>(){btnMezcla1,btnMezcla2,btnMezcla3, btnMezcla4};
            int iBanMezcla = 0;
            foreach (Button btn in arrayButtonMezcla) {
                if (banMezclaAnimationStop [iBanMezcla])
                    btn.GetComponent<Animator> ().Play ("AnimationMala" + (iBanMezcla + 1) + "Pr3");
                else {
                    btn.GetComponent<Animator> ().Play ("AnimationBien" + (iBanMezcla + 1) + "Pr3");
                }
                iBanMezcla++;
            }

            EnableRealizarAtaque();
        }
        private void EnableRealizarAtaque()
        {
            goAtaque.SetActive(true);
            goZonaPreparacion.SetActive(false);
            contPocionServidas = 0;

            btnMezcla1.enabled = true;
            btnMezcla1.onClick.AddListener(delegate ()
            {
                CheckAttack(btnMezcla1);
                    btnMezcla1.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla1.onClick.RemoveAllListeners();
            });

            btnMezcla2.enabled = true;
            btnMezcla2.onClick.AddListener(delegate ()
            {
                CheckAttack(btnMezcla2);
                    btnMezcla2.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla2.onClick.RemoveAllListeners();
            });

            btnMezcla3.enabled = true;
            btnMezcla3.onClick.AddListener(delegate ()
            {
                CheckAttack(btnMezcla3);
                    btnMezcla3.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla3.onClick.RemoveAllListeners();
            });

            btnMezcla4.enabled = true;
            btnMezcla4.onClick.AddListener(delegate ()
            {
                CheckAttack(btnMezcla4);
                    btnMezcla4.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla4.onClick.RemoveAllListeners();
            });
        }
        private void CheckAttack(Button btn)
        {
            numAtaquesTotal++;
            if (btn.name.Contains("Buena"))
            {
                numAtaquesEnemigo++;
                goAtaque.transform.FindChild("EnviarAtaque").gameObject.SetActive(true);
                animVidaEnemigo = goAtaque.transform.FindChild("EnviarAtaque").FindChild("imgVidaGrifo").GetComponent<Animator>();
                animVidaEnemigo.Play("AnimationEnviarAtaque" + numAtaquesEnemigo);
                StartCoroutine(WaitStopAnimation(2.5f, animVidaEnemigo, true));
            }
            else
            {
                numAtaquesAlumno++;
                goAtaque.transform.FindChild ("Dragon").GetComponent<Animator> ().Play ("AnimationDragonAtaque");
                StartCoroutine (waitAnimation());
            }
            btn.enabled = false;
            btn.onClick.RemoveAllListeners ();
        }

        private IEnumerator waitAnimation(){
            yield return new WaitForSeconds (1.0f);
            goAtaque.transform.FindChild("RecibirAtaque").gameObject.SetActive(true);
            animVidaAlumno = goAtaque.transform.FindChild("imgVidaAlumno").GetComponent<Animator>();
            animVidaAlumno.Play("AnimationRecibirAtaque" + numAtaquesAlumno);

            animMezclaGeneric = goAtaque.transform.FindChild("RecibirAtaque").GetComponent<Animator>();
            animMezclaGeneric.Play ("AnimationRecibirAtaque");
            if (numAtaquesTotal < 4) {
                StartCoroutine (WaitStopAnimation (1.2f, animMezclaGeneric, false));
            } else {
                StartCoroutine (WaitStopAnimation (1.2f, animMezclaGeneric, true));
            }
        }

        private IEnumerator WaitStopAnimation(float timeWait, Animator anim, bool option)
        {
            yield return new WaitForSeconds(timeWait);
            if (!option)
            {
                goAtaque.transform.FindChild("RecibirAtaque").gameObject.SetActive(false);
                goAtaque.transform.FindChild ("Dragon").GetComponent<Animator> ().Play ("AnimationDragon");
            }
            else
            {
                if (numAtaquesTotal >= 4)
                {
                    if (numAtaquesEnemigo >= 4 )//gano
                    {
                        goAtaque.transform.FindChild ("Dragon").GetComponent<Animator> ().Play ("AnimationDragonCaida");
                        StartCoroutine (WaitAnimationFinal ());
                    }
                    else//perdio
                    {
                        goEnemigo.SetActive(false);
                        goProfesorLast.SetActive(true);
                        if (estrellas >= 3) {
                            MostrarGloboProfesorLast("¡Bien hecho! Estoy impresionado, aunque éstos suelen volver, por un tiempo por lo menos nos veremos a ningún monstruo por la escuela.", 10.0f, 0);
                        } else {
                            MostrarGloboProfesorLast ("¡Bien hecho! Liberaste a la escuela del dragón, no dejes de preparar pociones para librarnos de más monstruos.", 10.0f,1);
                        }
                    }
                }
            }
        }

        private IEnumerator WaitAnimationFinal(){
            yield return new WaitForSeconds (1.5f);
            goAtaque.transform.FindChild ("Dragon").GetComponent<Animator>().enabled=false;
            int estrellas = PlayerPrefs.GetInt("estrellasODA08_ODA08");
            estrellas++;
            if (estrellas >= 3)
                estrellas = 3;
            Debug.Log ("estrellas :"+estrellas);
            PlayerPrefs.SetInt("estrellasODA08_ODA08",estrellas);

            GenericUi.PlayAnimationStar ();
            StartCoroutine(WaitStar ());
        }

        private IEnumerator  WaitStar()
        {
            yield return new WaitForSeconds(5.7f);
            GenericUi.NotResume ();
            Debug.Log ("popup");
            goEnemigo.SetActive(false);
            goProfesorLast.SetActive(true);
            estrellas = PlayerPrefs.GetInt ("estrellasODA08_ODA08");
            GenericUi.SoundManager.PlaySound("fuegos_artificiales", 1);
            Debug.Log ("fuegos");
            if (estrellas >= 3) {
                MostrarGloboProfesorLast("¡Bien hecho! Estoy impresionado, aunque éstos suelen volver, por un tiempo por lo menos nos veremos a ningún monstruo por la escuela.", 10.0f, 0);
            } else {
                MostrarGloboProfesorLast ("¡Bien hecho! Liberaste a la escuela del dragón, no dejes de preparar pociones para librarnos de más monstruos.", 10.0f,1);
            }
        }

        private void MostrarGloboProfesorLast(string msj, float timeMsj, int option)
        {
            Texto = goProfesorLast.transform.Find("GloboProfesor").Find("txtGloboProfesor").GetComponent<Text>();
            SetTextToAnimate(msj, timeMsj);
            switch (option)
            {
                case 0:
                Invoke("ShowLastPopUp", 15.7f);
                break;
                case 1:
                    Invoke("ShowLastPopUp", 11.0f);
                    break;
            }
        }

        private void ShowLastPopUp()
        {
            GenericUi.ShowInfoPopUp(
                "¡Sigamos con el reto final!",
                new ESequence
                {
                    Action = (Challenge.Action)Action.EndChallenge,
                    ChallengeNum = 3,
                    Event = Event.ClosePopUp
                }, 0.0f
            );
        }

        private void backToMenu()
        {
            goMenu.Mostrar ();
        }

        private void AnimationIngredient()
        {
            bool ban = false;
            foreach (var ingrediente in loRecetas[numPocionActual - 1].Mix)
            {
                if (ingrediente.Value.Type == EIngredient.TypeIngredient.Liquid)
                {
                    if (!(ingrediente.Value.Quantity * loRecetas[numPocionActual - 1].Dosage >= ingrediente.Value.Proportion - 5 && ingrediente.Value.Quantity * loRecetas[numPocionActual - 1].Dosage <= ingrediente.Value.Proportion + 5))
                    {
                        ban = true;
                        break;
                    }
                }
                else
                {
                    if (!(ingrediente.Value.Quantity * loRecetas[numPocionActual - 1].Dosage == ingrediente.Value.Proportion))
                    {
                        ban = true;
                        break;
                    }
                }
                if (ban) break;
            }
            Debug.Log("BAndera:" + ban);
            if (ban)
            {
                goMezclaAnimation.transform.Find("imgMezclaMala" + numPocionActual).gameObject.SetActive(true);
                animMezclaMala = goMezclaAnimation.transform.Find("imgMezclaMala" + numPocionActual).GetComponent<Animator>();
                animMezclaMala.Play("AnimacionMala" + numPocionActual+"r3");
            }
            else
            {
                goMezclaAnimation.transform.Find("imgMezclaBuena" + numPocionActual).gameObject.SetActive(true);
                animMezclaBuena = goMezclaAnimation.transform.Find("imgMezclaBuena" + numPocionActual).GetComponent<Animator>();
                animMezclaBuena.Play("AnimacionBien" + numPocionActual + "r3");
            }
            goPociones.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().enabled = false;
            switch (numPocionActual)
            {
            case 1:
                if (ban) {
                    btnMezcla1 = goMezclaAnimation.transform.Find ("imgMezclaMala1").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla1 = goMezclaAnimation.transform.Find ("imgMezclaBuena1").GetComponent<Button> ();
                }
                btnMezcla1.enabled = false;
                break;
            case 2:
                if (ban) {
                    btnMezcla2 = goMezclaAnimation.transform.Find ("imgMezclaMala2").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla2 = goMezclaAnimation.transform.Find ("imgMezclaBuena2").GetComponent<Button> ();
                }
                btnMezcla2.enabled = false;
                break;
            case 3:
                if (ban) {
                    btnMezcla3 = goMezclaAnimation.transform.Find ("imgMezclaMala3").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla3 = goMezclaAnimation.transform.Find ("imgMezclaBuena3").GetComponent<Button> ();
                }
                btnMezcla3.enabled = false;
                break;
            case 4:
                if (ban) {
                    btnMezcla4 = goMezclaAnimation.transform.Find ("imgMezclaMala4").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla4 = goMezclaAnimation.transform.Find ("imgMezclaBuena4").GetComponent<Button> ();
                }
                btnMezcla4.enabled = false;
                break;
            }
        }


        private void ZoomIn(string ingrediente)
        {
            if (numPocionActual > 0 && loRecetas[numPocionActual - 1].Mix.ContainsKey(ingrediente))
            {
                esZoomInTerminado = false;
                ingredienteActual = ingrediente;
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = true;
                animZoom = goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>();
                Debug.Log("Animationimg" + ingrediente);
                animZoom.Play("Animationimg"+ingrediente, -1, 0f);
                animZoom.speed = 1f;
                GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/zoom", 1);
            }
        }

        private void ZoomOut(string ingrediente)
        {
            esZoomInTerminado = true;

            if (loRecetas[numPocionActual - 1].Mix[ingrediente].Type == EIngredient.TypeIngredient.Liquid)
            {
                imgAreaInteractiva.color = loRecetas[numPocionActual - 1].Mix[ingrediente].Color;
                goMedidorLiquidos.SetActive(true);
            }
            else
                goMedidorSolidos.SetActive(true);
        }
       
        /// <summary>
        /// Execute a serie of action/event of the sequence, after an specified time
        /// </summary>
        /// <param name="sequence">The action/event to execute</param>
        /// <param name="waitingTime">Waiting time to launch the action/event of the sequence</param>
        /// <returns></returns>
        internal override IEnumerator Execute(ESequence sequence, float waitingTime)
        {
            yield return new WaitForSeconds(waitingTime);

            switch (sequence.Event)
            {
                case Event.ClosePopUp:
                    ClosePopUp((Action)sequence.Action);
                    break;
            }
        }

        /// <summary>
        /// Set the challange to its initial state
        /// </summary>
        protected override void ReStart()
        {
            //region Reset objects to initial state

            foreach (string ingrediente in lsIngredientes)
            {
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSprite").GetComponent<Image>().enabled = false;
                goIngredientes.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillo").GetComponent<Image>().enabled = false;
            }

            goPociones.transform.Find("btnPocion1").GetComponent<Button>().enabled = true;
            goPociones.transform.Find("btnPocion2").GetComponent<Button>().enabled = true;
            goPociones.transform.Find("btnPocion3").GetComponent<Button>().enabled = true;
            goPociones.transform.Find("btnPocion4").GetComponent<Button>().enabled = true;

            txtProporcionSolido.text = "0";
            txtReceta.text = string.Empty;

            goMedidorLiquidos.SetActive(false);
            goMedidorSolidos.SetActive(false);
            goZonaBloqueo.SetActive(true);
            goMezcla.SetActive(false);
            goReceta.SetActive(false);
            goPociones.SetActive(true);
            goEnemigo.SetActive(false);
            goLaboratorio.SetActive(false);
            goGloboProfesor.SetActive(false);
            goProfesor.SetActive(false);
            goInicio.SetActive(true);

            animPuerta.speed = 0f;

            esRetoActivo = false;
            esZoomInTerminado = false;
            sldMedidorLiquidos.value = 0;
            numIngredientesServidos = 0;
            numPocionActual = 0;
            ingredienteActual = string.Empty;
            proporcionSolido = string.Empty;

            contPocionServidas = 0;
            numAtaquesEnemigo = 0;
            numAtaquesAlumno = 0;
            numAtaquesTotal = 0;
            banPopUp = false;
            banMezclaAnimationStop = new bool[4];
            for (int i = 0; i < 4; i++)
            {
                banMezclaAnimationStop[i] = false;
            }
            goProfesorLast.SetActive(false);
            goAtaque.SetActive(false);
            goZonaPreparacion.SetActive(true);

            for (int i = 1; i < 5; i++)
                goMezclaAnimation.transform.FindChild("imgMezclaBuena" + i).gameObject.SetActive(false);

            for (int i = 1; i < 5; i++)
                goMezclaAnimation.transform.FindChild("imgMezclaMala" + i).gameObject.SetActive(false);

            goAtaque.transform.FindChild("EnviarAtaque").FindChild("imgVidaGrifo").GetComponent<Animator>().Play("AnimationDefaultEnemigo");

            goAtaque.transform.FindChild("RecibirAtaque").GetComponent<Animator>().Stop();
            goAtaque.transform.FindChild("imgVidaAlumno").GetComponent<Animator>().Play("AnimationDefault");
            btnMezcla1.onClick.RemoveAllListeners();
            btnMezcla2.onClick.RemoveAllListeners();
            btnMezcla3.onClick.RemoveAllListeners();
            btnMezcla4.onClick.RemoveAllListeners();
            
            InicializarPociones();

            //endregion
        }

        /// <summary>
        /// Set the state of the sequence
        /// </summary>
        /// <param name="state">The new state of the sequence</param>
        internal override void SetState(State state)
        {
            switch (state)
            {
                case State.Initial:
                    ReStart();
                    break;
                case State.Playing:
                    break;
                case State.Finished:
                    break;
            }
        }


        public int GenerarCantidadIngrediente(EIngredient.TypeIngredient tipoIngrediente)
        {
            int cantidad = 0;
            switch (tipoIngrediente)
            {
            case EIngredient.TypeIngredient.Liquid:
                cantidad = rndLiquidos.Next(5, 26);
                break;
            case EIngredient.TypeIngredient.Solid:
                cantidad = rndSolidos.Next(5, 251);
                break;
            }
            Debug.Log ("cantidad: "+(cantidad - cantidad % 5) );
            return cantidad - cantidad % 5;
        }

        public int GetDosage(){
            rndDosis = new System.Random();
            return 1;//rndDosis.Next (1, 5);
        }

    }
}


