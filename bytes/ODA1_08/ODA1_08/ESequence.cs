﻿namespace Assets.Scripts
{
	public class ESequence
	{
		#region Properties

		public Challenge.Action Action { get; set; }
		public int ChallengeNum { get; set; }
		public Challenge.Event Event { get; set; }
		public Challenge.State State { get; set; }

		#endregion
	}
}
