﻿using UnityEngine;

namespace Assets.Scripts
{
	public class EIngredient
	{
        public enum TypeIngredient
		{
			Liquid, Solid
		}
        public Color Color { get; set; }
        public int Proportion { get; set; }
        public int Quantity { get; set; }
        public TypeIngredient Type { get; set; }
	}
}
