﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Oda08Reto2 : Challenge
    {
        //region Enumeration
        public new enum Action
        {
            ShowEnemy, ShowProfessor, EndChallenge
        }
        //region Attributes

        private Oda08Indice goMenu;
        private Oda08Reto3 goReto3;
        private Animator animPuerta, animZoom, animMezclaBuena, animMezclaMala, animMezclaGeneric, animVidaEnemigo, animVidaAlumno;
        private bool esRetoActivo, esZoomInTerminado, banPopUp;
        private Button btnPuertar2, btnPocion1r2, btnPocion2r2, btnPocion3r2, btnPocion4r2, btnCapaToreror2, btnRioQueCantar2, btnPezuiaDeToror2, btnEsenciaDeClavelr2, btnLaberintor2, btnHadaNegrar2, btnCabelloDeSansonr2, btnLantoDeHerculesr2,
            btnSalivaPieGrander2, btnSalivaCaracolr2, btnPozoDeLaPerdicionr2, btnCuernoUnicornior2, btnTierraDeCementerior2, btnCapaMerilinr2, btnCaparazonMolidor2,btnCapulloMolidor2, btnAceptarLiquidosr2,
            btnAceptarSolidosr2, btnBorrarSolidosr2, btnTeclado0r2, btnTeclado1r2, btnTeclado2r2, btnTeclado3r2, btnTeclado4r2, btnTeclado5r2, btnTeclado6r2, btnTeclado7r2, btnTeclado8r2, btnTeclado9r2,
            btnMezcla1, btnMezcla2, btnMezcla3, btnMezcla4;
        private UnityEngine.GameObject goInicio, goProfesorr2, goProfesorLast, goGloboProfesorr2, goLaboratorior2, goEnemigor2, goPocionesr2, goRecetar2, goMezclar2,goAtaquer2, goMezclaAnimationr2, goZonaPreparacionr2, goIngredientesr2, goZonaBloqueor2, goMedidorLiquidosr2, goMedidorSolidosr2;
        private Image imgInterfazr2, imgRecetar2, imgAreaInteractiva;
        private bool[] banMezclaAnimationStop;
        private int numPocionActual, numIngredientesServidos, contPocionServidas, numAtaquesEnemigo, numAtaquesAlumno, numAtaquesTotal, estrellas;
        private List<string> lsIngredientesr2;
        private List<ERecipe> loRecetasr2;
        private Slider sldMedidorLiquidosr2;
        private string ingredienteActual, proporcionSolido;
        private Text txtRecetar2, txtProporcionSolidor2;
        private Button btnMezclarr2;
        public System.Random rndDosis,rndLiquidos, rndSolidos;
        //region Constructor
        public Oda08Reto2() : base("Reto2"){}
        // Use this for initialization
		protected override void Start()
        {
            //region Object instances

            lsIngredientesr2 = new List<string> {
                "LimaduraDeHierror2",
                "CapaToreror2", "RioQueCantar2",
                "PezuiaDeToror2", "EsenciaDeClavelr2",
                "Laberintor2", "HadaNegrar2",
                "CabelloDeSansonr2", "LlantoDeHerculesr2",
                "SalivaPieGrander2", "SalivaCaracolr2",
                "PozoDeLaPerdicionr2", "CuernoUnicornior2","CaparazonMolidor2","CapulloMolidor2",
                "TierraDeCementerior2", "CapaMerlinr2"
            };
            contPocionServidas = 0;
            numAtaquesEnemigo = 0;
            numAtaquesAlumno = 0;
            numAtaquesTotal = 0;
            //test
            banPopUp = false;
            banMezclaAnimationStop = new bool[4];
            for (int i = 0; i < 4; i++) {
                banMezclaAnimationStop [i] = false;
            }
            rndLiquidos = new System.Random();
            rndSolidos = new System.Random();
            rndDosis = new System.Random ();


            goMenu = transform.GetComponent<Oda08Indice>();
            goReto3 = transform.GetComponent<Oda08Reto3>();

            goInicio = Canvas.transform.Find("Inicio").gameObject;
            goLaboratorior2 = Canvas.transform.Find("Laboratorior2").gameObject;

            btnPuertar2 = goInicio.transform.Find("btnPuertar2").GetComponent<Button>();
            goProfesorr2 = goInicio.transform.Find("Profesorr2").gameObject;

            goEnemigor2 = goLaboratorior2.transform.Find("Enemigor2").gameObject;
            goProfesorLast = goLaboratorior2.transform.Find("Profesor").gameObject;

            animPuerta = btnPuertar2.GetComponent<Animator>();
            animPuerta.speed = 0f;
            goGloboProfesorr2 = goProfesorr2.transform.Find("GloboProfesorr2").gameObject;

            imgInterfazr2 = goEnemigor2.transform.Find("imgInterfazr2").GetComponent<Image>();
            goZonaPreparacionr2 = goEnemigor2.transform.Find("ZonaPreparacionr2").gameObject;

            goPocionesr2 = imgInterfazr2.transform.Find("Pocionesr2").gameObject;
            goRecetar2 = imgInterfazr2.transform.Find("Recetar2").gameObject;
            goMezclar2 = imgInterfazr2.transform.Find("Mezclar2").gameObject;
            goAtaquer2 = imgInterfazr2.transform.Find("Ataque").gameObject;
            goMezclaAnimationr2 = imgInterfazr2.transform.Find("MezclaAnimation").gameObject;
            btnMezcla1 = goMezclaAnimationr2.transform.FindChild ("b1").GetComponent<Button> ();
            btnMezcla2 = goMezclaAnimationr2.transform.FindChild ("b2").GetComponent<Button> ();
            btnMezcla3 = goMezclaAnimationr2.transform.FindChild ("b3").GetComponent<Button> ();
            btnMezcla4 = goMezclaAnimationr2.transform.FindChild ("b4").GetComponent<Button> ();

            btnPocion1r2 = goPocionesr2.transform.Find("btnPocion1").GetComponent<Button>();
            btnPocion2r2 = goPocionesr2.transform.Find("btnPocion2").GetComponent<Button>();
            btnPocion3r2 = goPocionesr2.transform.Find("btnPocion3").GetComponent<Button>();
            btnPocion4r2 = goPocionesr2.transform.Find("btnPocion4").GetComponent<Button>();

            btnMezclarr2 = goMezclar2.transform.Find("btnMezclarr2").GetComponent<Button>();

            imgRecetar2 = goRecetar2.transform.Find("imgRecetar2").GetComponent<Image>();

            txtRecetar2 = imgRecetar2.transform.Find("txtRecetar2").GetComponent<Text>();

            goIngredientesr2 = goZonaPreparacionr2.transform.Find("Ingredientesr2").gameObject;
            goZonaBloqueor2 = goZonaPreparacionr2.transform.Find("ZonaBloqueor2").gameObject;

            btnCapaToreror2 = goIngredientesr2.transform.Find("btnCapaToreror2").GetComponent<Button>();
            btnRioQueCantar2 = goIngredientesr2.transform.Find("btnRioQueCantar2").GetComponent<Button>();
            btnPezuiaDeToror2 = goIngredientesr2.transform.Find("btnPezuiaDeToror2").GetComponent<Button>();
            btnEsenciaDeClavelr2 = goIngredientesr2.transform.Find("btnEsenciaDeClavelr2").GetComponent<Button>();
            btnLaberintor2 = goIngredientesr2.transform.Find("btnLaberintor2").GetComponent<Button>();
            btnHadaNegrar2 = goIngredientesr2.transform.Find("btnHadaNegrar2").GetComponent<Button>();
            btnCabelloDeSansonr2 = goIngredientesr2.transform.Find("btnCabelloDeSansonr2").GetComponent<Button>();
            btnLantoDeHerculesr2 = goIngredientesr2.transform.Find("btnLlantoDeHerculesr2").GetComponent<Button>();
            btnSalivaPieGrander2 = goIngredientesr2.transform.Find("btnSalivaPieGrander2").GetComponent<Button>();
            btnSalivaCaracolr2 = goIngredientesr2.transform.Find("btnSalivaCaracolr2").GetComponent<Button>();
            btnPozoDeLaPerdicionr2 = goIngredientesr2.transform.Find("btnPozoDeLaPerdicionr2").GetComponent<Button>();
            btnCuernoUnicornior2 = goIngredientesr2.transform.Find("btnCuernoUnicornior2").GetComponent<Button>();
            btnTierraDeCementerior2 = goIngredientesr2.transform.Find("btnTierraDeCementerior2").GetComponent<Button>();
            btnCapaMerilinr2 = goIngredientesr2.transform.Find("btnCapaMerlinr2").GetComponent<Button>();

            goIngredientesr2.transform.Find("imgCapaToreror2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCapaToreror2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgRioQueCantar2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgPezuiaDeToror2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgEsenciaDeClavelr2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgLaberintor2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgHadaNegrar2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCabelloDeSansonr2").GetComponent<Image>().enabled = false;

            goIngredientesr2.transform.Find("imgLlantoDeHerculesr2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgSalivaPieGrander2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgSalivaCaracolr2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgPozoDeLaPerdicionr2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCuernoUnicornior2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCaparazonMolidor2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCapulloMolidor2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgTierraDeCementerior2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("imgCapaMerlinr2").GetComponent<Image>().enabled = false;
           
            foreach (string ingrediente in lsIngredientesr2)
            {
                Debug.Log ("NOmbre de ingrediente: "+ingrediente);
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSpriter2").GetComponent<Image>().enabled = false;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillor2").GetComponent<Image>().enabled = false;
                Debug.Log("nombreIngrediente: "+ ingrediente);
            }

            goMedidorLiquidosr2 = goZonaPreparacionr2.transform.Find("MedidorLiquidosr2").gameObject;
            goMedidorSolidosr2 = goZonaPreparacionr2.transform.Find("MedidorSolidosr2").gameObject;

            btnAceptarLiquidosr2 = goMedidorLiquidosr2.transform.Find("btnAceptarr2").GetComponent<Button>();
            sldMedidorLiquidosr2 = goMedidorLiquidosr2.transform.Find("imgProbetar2").GetComponent<Image>().transform.Find("sldMedidorr2").GetComponent<Slider>();

            imgAreaInteractiva = sldMedidorLiquidosr2.GetComponentsInChildren<Image>().FirstOrDefault(item => item.name == "imgAreaInteractiva");

            txtProporcionSolidor2 = goMedidorSolidosr2.transform.Find("imgBascular2").GetComponent<Image>().transform.Find("txtBascular2").GetComponent<Text>();
            btnAceptarSolidosr2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnAceptarr2").GetComponent<Button>();
            btnBorrarSolidosr2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnBorrarr2").GetComponent<Button>();
            btnTeclado0r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla0r2").GetComponent<Button>();
            btnTeclado1r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla1r2").GetComponent<Button>();
            btnTeclado2r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla2r2").GetComponent<Button>();
            btnTeclado3r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla3r2").GetComponent<Button>();
            btnTeclado4r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla4r2").GetComponent<Button>();
            btnTeclado5r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla5r2").GetComponent<Button>();
            btnTeclado6r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla6r2").GetComponent<Button>();
            btnTeclado7r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla7r2").GetComponent<Button>();
            btnTeclado8r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla8r2").GetComponent<Button>();
            btnTeclado9r2 = goMedidorSolidosr2.transform.Find("imgTeclador2").GetComponent<Image>().transform.Find("btnTecla9r2").GetComponent<Button>();
            
            //region Event listeners

            btnPuertar2.onClick.AddListener(MostrarLaboratorio);
            btnPocion1r2.onClick.AddListener(delegate
            {
                MostrarReceta(1);
                HabilitarZonaPreparacion();
                contPocionServidas++;
            });
            btnPocion2r2.onClick.AddListener(delegate
            {
                MostrarReceta(2);
                HabilitarZonaPreparacion();
                contPocionServidas++;
            });
            btnPocion3r2.onClick.AddListener(delegate
            {
                MostrarReceta(3);
                HabilitarZonaPreparacion();
                contPocionServidas++;
            });
            btnPocion4r2.onClick.AddListener(delegate
            {
                MostrarReceta(4);
                HabilitarZonaPreparacion();
                contPocionServidas++;
            });
            btnMezclarr2.onClick.AddListener(MezclarIngredientes);


            btnCapaToreror2.onClick.AddListener(delegate
            {
                ZoomIn("CapaToreror2");
            });
            btnRioQueCantar2.onClick.AddListener(delegate
            {
                ZoomIn("RioQueCantar2");
            });
            btnPezuiaDeToror2.onClick.AddListener(delegate
            {
                ZoomIn("PezuiaDeToror2");
            });
            btnEsenciaDeClavelr2.onClick.AddListener(delegate
            {
                ZoomIn("EsenciaDeClavelr2");
            });
            btnLaberintor2.onClick.AddListener(delegate
            {
                ZoomIn("Laberintor2");
            });
            btnHadaNegrar2.onClick.AddListener(delegate
            {
                ZoomIn("HadaNegrar2");
            });
            btnCabelloDeSansonr2.onClick.AddListener(delegate
            {
                ZoomIn("CabelloDeSansonr2");
            });
            btnLantoDeHerculesr2.onClick.AddListener(delegate
            {
                ZoomIn("LlantoDeHerculesr2");
            });
            btnSalivaPieGrander2.onClick.AddListener(delegate
            {
                ZoomIn("SalivaPieGrander2");
            });
            btnSalivaCaracolr2.onClick.AddListener(delegate
            {
                ZoomIn("SalivaCaracolr2");
            });
            btnPozoDeLaPerdicionr2.onClick.AddListener(delegate
            {
                ZoomIn("PozoDeLaPerdicionr2");
            });
            btnCuernoUnicornior2.onClick.AddListener(delegate
            {
                ZoomIn("CuernoUnicornior2");
            });
            btnTierraDeCementerior2.onClick.AddListener(delegate
            {
                ZoomIn("TierraDeCementerior2");
            });
            btnCapaMerilinr2.onClick.AddListener(delegate
            {
                    ZoomIn("CapaMerlinr2");
            });
            btnAceptarLiquidosr2.onClick.AddListener(GuardarProporcion);
            btnAceptarSolidosr2.onClick.AddListener(GuardarProporcion);
            btnBorrarSolidosr2.onClick.AddListener(BorrarProporcion);
            btnTeclado0r2.onClick.AddListener(delegate
            {
                SumarProporcion(0);
            });
            btnTeclado1r2.onClick.AddListener(delegate
            {
                SumarProporcion(1);
            });
            btnTeclado2r2.onClick.AddListener(delegate
            {
                SumarProporcion(2);
            });
            btnTeclado3r2.onClick.AddListener(delegate
            {
                SumarProporcion(3);
            });
            btnTeclado4r2.onClick.AddListener(delegate
            {
                SumarProporcion(4);
            });
            btnTeclado5r2.onClick.AddListener(delegate
            {
                SumarProporcion(5);
            });
            btnTeclado6r2.onClick.AddListener(delegate
            {
                SumarProporcion(6);
            });
            btnTeclado7r2.onClick.AddListener(delegate
            {
                SumarProporcion(7);
            });
            btnTeclado8r2.onClick.AddListener(delegate
            {
                SumarProporcion(8);
            });
            btnTeclado9r2.onClick.AddListener(delegate
            {
                SumarProporcion(9);
            });
            InicializarPociones();
        }

        // Update is called once per frame
        protected override void Update()
        {
            //region Initial state

            if (Instance.activeInHierarchy)
            {
                if (!esRetoActivo)
                {
                    GenericUi.ShowInfoPopUp(
                        "Veamos si puedes impresionar al exigente profesor Galaguer con tus pociones.",
                        new ESequence
                        {
                            Action = (Challenge.Action)Action.ShowProfessor,
                            ChallengeNum = 2,
                            Event = Event.ClosePopUp
                        }, 0.0f
                    );
                    esRetoActivo = true;
                }
                else
                {
                    if (!string.IsNullOrEmpty(ingredienteActual) && !esZoomInTerminado &&
                        animZoom.GetCurrentAnimatorStateInfo(0).IsName("Animationimg"+ingredienteActual) &&
                        animZoom.GetCurrentAnimatorStateInfo(0).normalizedTime % animZoom.GetCurrentAnimatorStateInfo(0).length > 1.0f)
                        ZoomOut(ingredienteActual);
                }
            }
            else
                esRetoActivo = false;
            AnimateText();
        }

        private void MostrarGloboProfesorLast(string msj, float timeMsj, int option)
        {
            Texto = goProfesorLast.transform.Find("GloboProfesor").Find("txtGloboProfesor").GetComponent<Text>();
            SetTextToAnimate(msj, timeMsj);
            switch (option)
            {
                case 0:
                    break;
                case 1:
                    Invoke("ShowLastPopUp", 8.5f);
                    break;
                case 2:
                    Invoke("backToMenu", timeMsj + 2.2f);
                    break;
            }
        }

        private void ShowLastPopUp()
        {
            GenericUi.ShowInfoPopUp(
                "¡Sigamos con el segundo reto!",
                new ESequence
                {
                    Action = (Challenge.Action)Action.EndChallenge,
                    ChallengeNum = 2,
                    Event = Event.ClosePopUp
                }, 0.0f
            );
        }
        private void backToMenu()
        {
            goMenu.Mostrar ();
        }
        private void InicializarPociones()
        {
            //ReSharper disable once UseObjectOrCollectionInitializer
            loRecetasr2 = new List<ERecipe>();

            //region Initializing potion mix
            Debug.Log("iniciar posciones");
            loRecetasr2.Add(new ERecipe
            {
                Dosage = GetDosage(),
                Mix = new Dictionary<string, EIngredient>
                {
                    {"CapaToreror2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                /*    {"RioQueCantar2", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"PezuiaDeToror2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"EsenciaDeClavelr2", new EIngredient {Color = new Color32(0x99, 0xE5, 0xA3, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"Laberintor2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}} */
                }

            });
            loRecetasr2.Add(new ERecipe
            {
                Dosage = GetDosage(),
                Mix = new Dictionary<string, EIngredient>
                {
                    {"HadaNegrar2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                     /*{"CabelloDeSansonr2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"LlantoDeHerculesr2", new EIngredient {Color = new Color32(0x04, 0xF0, 0xFF, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"SalivaPieGrander2", new EIngredient {Color = new Color32(0x00, 0xED, 0x00, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}} */
                }

            });
            loRecetasr2.Add(new ERecipe
            {
                Dosage = GetDosage(),
                Mix = new Dictionary<string, EIngredient>
                {
                    {"SalivaCaracolr2", new EIngredient {Color = new Color32 (0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},/*
                     {"Laberintor2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"PozoDeLaPerdicionr2", new EIngredient {Color = new Color32 (0x16, 0xD2, 0xD9, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"CuernoUnicornior2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"TierraDeCementerior2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}} */
                }
            });
            loRecetasr2.Add(new ERecipe
            {
                Dosage = GetDosage(),
                Mix = new Dictionary<string, EIngredient>
                {
                    {"HadaNegrar2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},/*
                     {"CapaMerlinr2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"EsenciaDeClavelr2", new EIngredient {Color = new Color32 (0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}},
                    {"CuernoUnicornior2", new EIngredient {Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Solid), Type = EIngredient.TypeIngredient.Solid}},
                    {"SalivaPieGrander2", new EIngredient {Color = new Color32 (0x4A, 0x5A, 0x5B, 0xFF), Proportion = 0, Quantity = GenerarCantidadIngrediente(EIngredient.TypeIngredient.Liquid), Type = EIngredient.TypeIngredient.Liquid}}, */
                }
            });
        }
        
        private void ClosePopUp(Action action)
        {
            switch (action)
            {
                //region Cases
                case Action.ShowEnemy:
                    goEnemigor2.SetActive(true);
                    break;
                case Action.ShowProfessor:
                    goProfesorr2.SetActive(true);
                    animPuerta.Play("Puerta");
                    animPuerta.speed = 1f;
                    Invoke("MostrarGloboProfesorr2", 0.45f);
                    break;
                case Action.EndChallenge:
                ReStart ();
                    this.Active(false);
                    goReto3.Active(true);
                    break;
            }

            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void MostrarGloboProfesorr2()
        {
            goGloboProfesorr2.SetActive(true);
            Texto = goGloboProfesorr2.transform.Find("txtGloboProfesorr2").GetComponent<Text>();
            SetTextToAnimate("Toca la puerta para seguir con el curso de Brebajes y pociones. ", 4.0f);
            Invoke("EnableDoor", 4.0f);
        }

        private void EnableDoor()
        {
            banPopUp = true;
        }

        private void MostrarLaboratorio()
        {
            if (!banPopUp) return;
            goLaboratorior2.SetActive(true);
            goInicio.SetActive(false);
            string msjPopUP = "";
            int numEstrellas = PlayerPrefs.GetInt("estrellasODA08_ODA08");
            Debug.Log ("strellas en reto 2: "+numEstrellas);
            if (numEstrellas == 0)
            {
                msjPopUP = "Prueba 2: vencer al Minotauro.\n Selecciona y prepara las pociones, indica los gramos o mililitros que corresponden a la dosis necesaria para que funcionen las pociones."; 
            }
            else if (numEstrellas >= 1)
            {
                msjPopUP = "Prueba 2: vencer al Minotauro.\n Gracias a tu estrella tienes 1 minuto extra para preparar las pociones.";
                GenericUi.Time = 359;
            }

            GenericUi.ShowInfoPopUp(
                msjPopUP,
                new ESequence
                {
                    Action = (Challenge.Action)Action.ShowEnemy,
                    ChallengeNum = 2,
                    Event = Event.ClosePopUp
                }, 0.0f
            );
        }

        private void MostrarReceta(int numPocion)
        {
            numPocionActual = numPocion;
            goPocionesr2.SetActive(false);
            goRecetar2.SetActive(true);
            switch (numPocion)
            {
                //region Recetas
                case 1:
                    txtRecetar2.text =
                        "<size=24><b>TAURUS:</b></size> Hace que desaparezca la parte de toro.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                        
                        "	"+loRecetasr2[0].Mix["CapaToreror2" ].Quantity + " gr polvo de capota de torero \n" +/*
                         "	"+loRecetasr2[0].Mix["RioQueCantar2"].Quantity+" ml de agua del río que canta \n" +
                        "	" + loRecetasr2[0].Mix["PezuiaDeToror2"].Quantity +" gr de pezuña de toro \n" +
                        "	" + loRecetasr2[0].Mix["EsenciaDeClavelr2"].Quantity + " ml de esencia de clavel \n" +
                        "	" + loRecetasr2[0].Mix["Laberintor2"].Quantity + " gr de polvo del laberinto sin fin \n\n" + */
                        "<b>PARA QUE FUNCIONE EN EL MINOTAURO NECESITAS: " + loRecetasr2[numPocion-1].Dosage + " DOSIS.</b>";
                    break;
                case 2:
                    txtRecetar2.text =
                        "<size=24><b>IMPOTENS:</b></size> : Quita la fuerza.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	"+loRecetasr2[1].Mix["HadaNegrar2"].Quantity+" gr de polvo de Hada Negra \n" +/*
                         "	" + loRecetasr2[1].Mix["CabelloDeSansonr2"].Quantity + " gr del cabello de Sansón \n" +
                        "	" + loRecetasr2[1].Mix["LlantoDeHerculesr2"].Quantity + " ml llanto de Hércules \n" +
                        "	" + loRecetasr2[1].Mix["SalivaPieGrander2"].Quantity + " ml de saliva de Pie Grande \n\n" + */
                        "<b>PARA QUE FUNCIONE EN EL MINOTAURO NECESITAS: " + loRecetasr2[numPocion - 1].Dosage + " DOSIS.</b>";
                    break;
                case 3:
                    txtRecetar2.text =
                        "<size=24><b> TARDUS:</b></size> Aletarga al oponente.\n\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	" + loRecetasr2[2].Mix["SalivaCaracolr2"].Quantity + " ml de saliva de caracol\n" +/*
                         "	" + loRecetasr2[2].Mix["Laberintor2"].Quantity + " gr de laberinto sin fin\n" +
                        "	" + loRecetasr2[2].Mix["PozoDeLaPerdicionr2"].Quantity + " ml de agua estancada del pozo de la perdición\n" +
                        "	" + loRecetasr2[2].Mix["CuernoUnicornior2"].Quantity + " gr de cuerno de Unicornio molido\n" +
                        "	" + loRecetasr2[2].Mix["TierraDeCementerior2"].Quantity + " gr de tierra de cementerio \n" + */
                        "<b>PARA QUE FUNCIONE EN EL MINOTAURO NECESITAS: " + loRecetasr2[numPocion - 1].Dosage + " DOSIS.</b>";
                    break;
                case 4:
                    txtRecetar2.text =
                        "<size=24><b> DEPELLENDAM:</b></size> Desvanece al enemigo cuando se encuentra débil.\n" +
                        "Para <b>una</b> dosis:\n" +
                        "	" + loRecetasr2[3].Mix["HadaNegrar2"].Quantity + " gr de polvo de Hada Negra\n" +/*
                         "	" + loRecetasr2[3].Mix["CapaMerlinr2"].Quantity + " gr de tela de la capa de Merlín\n" +
                        "	" + loRecetasr2[3].Mix["EsenciaDeClavelr2"].Quantity + " ml de esencia de clavel\n" +
                        "	" + loRecetasr2[3].Mix["CuernoUnicornior2"].Quantity + " gr de cuerno de Unicornio molido\n" +
                        "	" + loRecetasr2[3].Mix["SalivaPieGrander2"].Quantity + " ml de saliva de Pie Grande \n" + */
                        "<b>PARA QUE FUNCIONE EN EL MINOTAURO NECESITAS: " + loRecetasr2[numPocion - 1].Dosage + " DOSIS.</b>";
                    break;
            }
            foreach (string ingrediente in loRecetasr2[numPocionActual - 1].Mix.Keys)
            {
                Debug.Log ("Nombe de ingrediente: "+ingrediente);
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSpriter2").GetComponent<Image>().enabled = true;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillor2").GetComponent<Image>().enabled = true;
                Debug.Log("ingrediente " + ingrediente);
                Debug.Log("proporcion " + loRecetasr2[numPocionActual - 1].Mix[ingrediente].Proportion);
                Debug.Log("cantaidad " + loRecetasr2[numPocionActual - 1].Mix[ingrediente].Quantity);
            }
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void HabilitarZonaPreparacion()
        {
            goZonaBloqueor2.SetActive(false);
            GenericUi.Resume();
            GenericUi.ShowStart ();
        }

        private void SumarProporcion(int cantidad)
        {
            if (((cantidad == 0 && txtProporcionSolidor2.text != "0") || cantidad != 0) &&
                txtProporcionSolidor2.text.Length < 8)
            {
                proporcionSolido += cantidad;
                txtProporcionSolidor2.text = proporcionSolido;
                GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
            }
        }

        private void BorrarProporcion()
        {
            proporcionSolido = string.Empty;
            txtProporcionSolidor2.text = "0";
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
        }

        private void GuardarProporcion()
        {
            Debug.Log("ingrediente: " + ingredienteActual);
            if (loRecetasr2[numPocionActual - 1].Mix[ingredienteActual].Type == EIngredient.TypeIngredient.Liquid)
            {
                //region Guardar proporción de ingrediente líquido
                if ((int)sldMedidorLiquidosr2.value > 0)
                {
                    loRecetasr2[numPocionActual - 1].Mix[ingredienteActual].Proportion = Mathf.RoundToInt(sldMedidorLiquidosr2.value);
                    Debug.Log("Valor de liquido: " + Mathf.RoundToInt(sldMedidorLiquidosr2.value));
                    sldMedidorLiquidosr2.value = 0;
                    ContabilizarIngredienteServido();
                }
                goMedidorLiquidosr2.SetActive(false);
            }
            else
            {
                //region Guardar proporción de ingrediente sólido
                if (!string.IsNullOrEmpty(proporcionSolido))
                {
                    int proporcion = int.Parse(proporcionSolido);
                    if (proporcion != 0)
                    {
                        loRecetasr2[numPocionActual - 1].Mix[ingredienteActual].Proportion = proporcion;
                        ContabilizarIngredienteServido();
                    }
                    Debug.Log("porcion de solido: " + proporcion);
                    proporcionSolido = string.Empty;
                    txtProporcionSolidor2.text = "0";
                }
                goMedidorSolidosr2.SetActive(false);
            }
        }

        private void ContabilizarIngredienteServido()
        {
            goIngredientesr2.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgSpriter2").GetComponent<Image>().enabled = false;
            goIngredientesr2.transform.Find("btn" + ingredienteActual).GetComponent<Button>().transform.Find("imgBrillor2").GetComponent<Image>().enabled = false;
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);
            Debug.Log("ingre serv: "+ numIngredientesServidos);
            Debug.Log("count lista recebtas: "+ loRecetasr2[numPocionActual - 1].Mix.Count);
            if (++numIngredientesServidos == loRecetasr2[numPocionActual - 1].Mix.Count)
                MezclarIngredientes ();
        } 
        
        private void MezclarIngredientes()
        {
            GenericUi.SoundManager.PlaySound("audios-basicos/cerrar3", 1);

            goMezclar2.SetActive(false);
            goRecetar2.SetActive(false);
            goPocionesr2.SetActive(true);
            Debug.Log("pocion actual: " + numPocionActual);
            goPocionesr2.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().enabled = false;
            AnimationIngredient();
            ingredienteActual = string.Empty;
            numIngredientesServidos = 0;
            numPocionActual = 0;
            //test cambiar a 4
            if (contPocionServidas >=4)
            {
                StartCoroutine(WaitRealizarAtaque(2.2f));
            }
        }

        private void EnableRealizarAtaque()
        {
            goAtaquer2.SetActive(true);
            goZonaPreparacionr2.SetActive(false);
            contPocionServidas = 0;

            btnMezcla1.enabled = true;
            btnMezcla1.onClick.AddListener(delegate ()
            {
                CheckAttack(btnMezcla1);
                btnMezcla1.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla1.onClick.RemoveAllListeners();
            });
            btnMezcla2.enabled = true;
            btnMezcla2.onClick.AddListener(delegate ()
            {
                CheckAttack(btnMezcla2);
                btnMezcla2.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla2.onClick.RemoveAllListeners();
            });

            btnMezcla3.enabled = true;
            btnMezcla3.onClick.AddListener(delegate ()
            {
                CheckAttack(btnMezcla3);
                    btnMezcla3.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla3.onClick.RemoveAllListeners();
            });

            btnMezcla4.enabled = true;
            btnMezcla4.onClick.AddListener(delegate ()
            {
                CheckAttack(btnMezcla4);
                btnMezcla4.GetComponent<Animator>().Play("AnimationStop");
                    btnMezcla4.onClick.RemoveAllListeners();
            });
        }


        private void CheckAttack(Button btn)
        {
            numAtaquesTotal++;
            Debug.Log ("verificando si es buena o mala");
            if (btn.name.Contains("Buena"))
            {
                numAtaquesEnemigo++;
                goAtaquer2.transform.FindChild("EnviarAtaque").gameObject.SetActive(true);
                animVidaEnemigo = goAtaquer2.transform.FindChild("EnviarAtaque").FindChild("imgVidaGrifo").GetComponent<Animator>();
                animVidaEnemigo.Play("AnimationEnviarAtaque" + numAtaquesEnemigo);
                StartCoroutine(WaitStopAnimation(2.5f, animVidaEnemigo, true));
                Debug.Log ("boton bueno");
            }
            else
            {
                numAtaquesAlumno++;
                goAtaquer2.transform.FindChild ("Minotauro").GetComponent<Animator> ().Play ("AnimationMinotauroAtaque");
                StartCoroutine (WaitAnimation());
                Debug.Log ("boton malo");
            }
            btn.enabled = false;
            btn.onClick.RemoveAllListeners ();
        }

        private IEnumerator WaitAnimation(){
            yield return new WaitForSeconds (1.0f);
            goAtaquer2.transform.FindChild("RecibirAtaque").gameObject.SetActive(true);
            animVidaAlumno = goAtaquer2.transform.FindChild("imgVidaAlumno").GetComponent<Animator>();
            animVidaAlumno.Play("AnimationRecibirAtaque" + numAtaquesAlumno);

            animMezclaGeneric = goAtaquer2.transform.FindChild("RecibirAtaque").GetComponent<Animator>();
            animMezclaGeneric.Play ("AnimationRecibirAtaque");
            Debug.Log ("entrando a animacion de espera de estrella");
            if (numAtaquesTotal < 4) {
                StartCoroutine (WaitStopAnimation (1.2f, animMezclaGeneric, false));
            } else {
                StartCoroutine (WaitStopAnimation (1.2f, animMezclaGeneric, true));
            }
        }

        private IEnumerator WaitStopAnimation(float timeWait, Animator anim, bool option)
        {
            yield return new WaitForSeconds(timeWait);
            if (!option)
            {
                goAtaquer2.transform.FindChild("RecibirAtaque").gameObject.SetActive(false);
                goAtaquer2.transform.FindChild ("Minotauro").GetComponent<Animator> ().Play ("AnimationMinotauro");
            }
            else
            { 
                if (numAtaquesTotal >= 4) { 
                    if (numAtaquesEnemigo >= 4) {
                        goAtaquer2.transform.FindChild ("Minotauro").GetComponent<Animator> ().Play ("AnimationMinotauroCaida");

                        Debug.Log ("entrando a estrellas");
                        StartCoroutine (WaitAnimationFinal());
                    } else {
                        goEnemigor2.SetActive (false);
                        goProfesorLast.SetActive (true);
                        MostrarGloboProfesorLast ("¡He tenido que salvarte! Las pociones no tienen la cantidad de ingredientes adecuada. Vuelve a intentarlo y fíjate bien en cuál es la dosis necesaria para cada poción.", 10.0f, 2);
                    }
                } 
            }
        }

        private IEnumerator WaitAnimationFinal(){
            yield return new WaitForSeconds (1.5f);
            goAtaquer2.transform.FindChild ("Minotauro").GetComponent<Animator> ().enabled = false;
            int estrellas = PlayerPrefs.GetInt ("estrellasODA08_ODA08");
            estrellas++;
            if (estrellas > 3)
                estrellas = 1;
            Debug.Log ("estrellas :" + estrellas);
            PlayerPrefs.SetInt ("estrellasODA08_ODA08", estrellas);

            GenericUi.PlayAnimationStar ();
            Debug.Log ("esperando estrella");
            StartCoroutine (WaitStar ());
        }

        private IEnumerator  WaitStar()
        {
            yield return new WaitForSeconds(5.7f);
            GenericUi.NotResume ();

            Debug.Log ("popup");
            goEnemigor2.SetActive(false);
            goProfesorLast.SetActive(true);
            MostrarGloboProfesorLast ("¡Bien hecho! Has superado tu primera prueba,  ¿tendrás el valor de enfrentar al siguiente monstruo?", 7.0f,1);
        }

        private IEnumerator WaitRealizarAtaque(float timeWait)
        {
            yield return new WaitForSeconds(timeWait);
            List<Button> arrayButtonMezcla = new List<Button>(){btnMezcla1,btnMezcla2,btnMezcla3, btnMezcla4};
            int iBanMezcla = 0;
            foreach (Button btn in arrayButtonMezcla) {
                if (banMezclaAnimationStop [iBanMezcla])
                    btn.GetComponent<Animator> ().Play ("AnimationMala" + (iBanMezcla + 1) + "Pr2");
                else {
                    btn.GetComponent<Animator> ().Play ("AnimationBien" + (iBanMezcla + 1) + "Pr2");
                }
                iBanMezcla++;
            }
            EnableRealizarAtaque();
        }

        private void AnimationIngredient()
        {
            bool ban = false;
            foreach (var ingrediente in loRecetasr2[numPocionActual - 1].Mix)
            {
                if (ingrediente.Value.Type == EIngredient.TypeIngredient.Liquid)
                {
                    if (!(ingrediente.Value.Quantity * loRecetasr2[numPocionActual - 1].Dosage >= ingrediente.Value.Proportion - 2 && ingrediente.Value.Quantity * loRecetasr2[numPocionActual - 1].Dosage <= ingrediente.Value.Proportion + 2))
                    {
                        ban = true;
                        break;
                    }
                }
                else
                {
                    if (!(ingrediente.Value.Quantity * loRecetasr2[numPocionActual - 1].Dosage == ingrediente.Value.Proportion))
                    {
                        ban = true;
                        break;
                    }
                }
                if (ban) break;
            }
            Debug.Log("BAndera:" + ban);
            if (ban)
            {
                goMezclaAnimationr2.transform.Find("imgMezclaMala" + numPocionActual).gameObject.SetActive(true);
                animMezclaMala = goMezclaAnimationr2.transform.Find("imgMezclaMala" + numPocionActual).GetComponent<Animator>();
                animMezclaMala.Play("AnimacionMala" + numPocionActual + "r2");
            }
            else
            {
                goMezclaAnimationr2.transform.Find("imgMezclaBuena" + numPocionActual ).gameObject.SetActive(true);
                Debug.Log("Posion: "+numPocionActual + "r2");
                animMezclaBuena = goMezclaAnimationr2.transform.Find("imgMezclaBuena" + numPocionActual ).GetComponent<Animator>();
                animMezclaBuena.Play("AnimacionBien" + numPocionActual+"r2");
            }
            goPocionesr2.transform.Find("btnPocion" + numPocionActual).GetComponent<Button>().enabled = false;
            switch (numPocionActual)
            {
            case 1:
                if (ban) {
                    btnMezcla1 = goMezclaAnimationr2.transform.Find ("imgMezclaMala1").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla1 = goMezclaAnimationr2.transform.Find ("imgMezclaBuena1").GetComponent<Button> ();
                }
                btnMezcla1.enabled = false;
                break;
            case 2:
                if (ban) {
                    btnMezcla2 = goMezclaAnimationr2.transform.Find ("imgMezclaMala2").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla2 = goMezclaAnimationr2.transform.Find ("imgMezclaBuena2").GetComponent<Button> ();
                }
                btnMezcla2.enabled = false;
                break;
            case 3:
                if (ban) {
                    btnMezcla3 = goMezclaAnimationr2.transform.Find ("imgMezclaMala3").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla3 = goMezclaAnimationr2.transform.Find ("imgMezclaBuena3").GetComponent<Button> ();
                }
                btnMezcla3.enabled = false;
                break;
            case 4:
                if (ban) {
                    btnMezcla4 = goMezclaAnimationr2.transform.Find ("imgMezclaMala4").GetComponent<Button> ();
                    banMezclaAnimationStop [numPocionActual - 1] = true;
                } else {
                    btnMezcla4 = goMezclaAnimationr2.transform.Find ("imgMezclaBuena4").GetComponent<Button> ();
                }
                btnMezcla4.enabled = false;
                break;
            }
        }
        
        private void ZoomIn(string ingrediente)
        {
            if (numPocionActual > 0 && loRecetasr2[numPocionActual - 1].Mix.ContainsKey(ingrediente))
            {
                esZoomInTerminado = false;
                ingredienteActual = ingrediente;
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = true;
                animZoom = goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>();
                Debug.Log("ingrediente enzoom: "+ingrediente);
                animZoom.Play("Animationimg"+ingrediente, -1, 0f);
                animZoom.speed = 1f;
                GenericUi.SoundManager.PlaySound("audio-brebajes-y-pociones/zoom", 1);
            }
        }

        private void ZoomOut(string ingrediente)
        {
            esZoomInTerminado = true;

            if (loRecetasr2[numPocionActual - 1].Mix[ingrediente].Type == EIngredient.TypeIngredient.Liquid)
            {
                imgAreaInteractiva.color = loRecetasr2[numPocionActual - 1].Mix[ingrediente].Color;
                goMedidorLiquidosr2.SetActive(true);
            }
            else
                goMedidorSolidosr2.SetActive(true);
        }

        //region Protected

        /// <summary>
        /// Execute a serie of action/event of the sequence, after an specified time
        /// </summary>
        /// <param name="sequence">The action/event to execute</param>
        /// <param name="waitingTime">Waiting time to launch the action/event of the sequence</param>
        /// <returns></returns>
        internal override IEnumerator Execute(ESequence sequence, float waitingTime)
        {
            yield return new WaitForSeconds(waitingTime);

            switch (sequence.Event)
            {
                case Event.ClosePopUp:
                    ClosePopUp((Action)sequence.Action);
                    break;
            }
        }

        /// <summary>
        /// Set the challange to its initial state
        /// </summary>
        protected override void ReStart()
        {
            //region Reset objects to initial state

            foreach (string ingrediente in lsIngredientesr2)
            {
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().enabled = false;
                goIngredientesr2.transform.Find("img" + ingrediente).GetComponent<Image>().GetComponent<Animator>().speed = 0f;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgSpriter2").GetComponent<Image>().enabled = false;
                goIngredientesr2.transform.Find("btn" + ingrediente).GetComponent<Button>().transform.Find("imgBrillor2").GetComponent<Image>().enabled = false;
            }

            goPocionesr2.transform.Find("btnPocion1").GetComponent<Button>().enabled = true;
            goPocionesr2.transform.Find("btnPocion2").GetComponent<Button>().enabled = true;
            goPocionesr2.transform.Find("btnPocion3").GetComponent<Button>().enabled = true;
            goPocionesr2.transform.Find("btnPocion4").GetComponent<Button>().enabled = true;

            txtProporcionSolidor2.text = "0";
            txtRecetar2.text = string.Empty;

            goMedidorLiquidosr2.SetActive(false);
            goMedidorSolidosr2.SetActive(false);
            goZonaBloqueor2.SetActive(true);
            goMezclar2.SetActive(false);
            goRecetar2.SetActive(false);
            goPocionesr2.SetActive(true);
            goEnemigor2.SetActive(false);
            goLaboratorior2.SetActive(false);
            goGloboProfesorr2.SetActive(false);
            goProfesorr2.SetActive(false);
            goInicio.SetActive(true);

            animPuerta.speed = 0f;

            esRetoActivo = false;
            esZoomInTerminado = false;
            sldMedidorLiquidosr2.value = 0;
            numIngredientesServidos = 0;
            numPocionActual = 0;
            ingredienteActual = string.Empty;
            proporcionSolido = string.Empty;

            contPocionServidas = 0;
            numAtaquesEnemigo = 0;
            numAtaquesAlumno = 0;
            numAtaquesTotal = 0;
            banPopUp = false;
            banMezclaAnimationStop = new bool[4];
            for (int i = 0; i < 4; i++)
            {
                banMezclaAnimationStop[i] = false;
            }
            goProfesorLast.SetActive(false);
            goAtaquer2.SetActive(false);
            goZonaPreparacionr2.SetActive(true);

            for (int i = 1; i < 5; i++)
                goMezclaAnimationr2.transform.FindChild("imgMezclaBuena" + i).gameObject.SetActive(false);

            for (int i = 1; i < 5; i++)
                goMezclaAnimationr2.transform.FindChild("imgMezclaMala" + i).gameObject.SetActive(false);

            goAtaquer2.transform.FindChild("EnviarAtaque").FindChild("imgVidaGrifo").GetComponent<Animator>().Play("AnimationDefaultEnemigo");

            goAtaquer2.transform.FindChild("RecibirAtaque").GetComponent<Animator>().Stop();
            goAtaquer2.transform.FindChild("imgVidaAlumno").GetComponent<Animator>().Play("AnimationDefault");
            btnMezcla1.onClick.RemoveAllListeners();
            btnMezcla2.onClick.RemoveAllListeners();
            btnMezcla3.onClick.RemoveAllListeners();
            btnMezcla4.onClick.RemoveAllListeners();

            InicializarPociones();
        }

        /// <summary>
        /// Set the state of the sequence
        /// </summary>
        /// <param name="state">The new state of the sequence</param>
        internal override void SetState(State state)
        {
            switch (state)
            {
                case State.Initial:
                    ReStart();
                    break;
                case State.Playing:
                    break;
                case State.Finished:
                    break;
            }
        }
        public int GenerarCantidadIngrediente(EIngredient.TypeIngredient tipoIngrediente)
        {
            int cantidad = 0;
            switch (tipoIngrediente)
            {
            case EIngredient.TypeIngredient.Liquid:
                cantidad = rndLiquidos.Next(5, 26);
                break;
            case EIngredient.TypeIngredient.Solid:
                cantidad = rndSolidos.Next(5, 251);
                break;
            }
            Debug.Log ("cantidad: "+(cantidad - cantidad % 5) );
            return cantidad - cantidad % 5;
        }

        public int GetDosage(){
            rndDosis = new System.Random();
            return 1;//rndDosis.Next (1, 5);
        }
    }
}